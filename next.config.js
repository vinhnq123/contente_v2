const path = require('path');

module.exports = () => ({
    i18n: {
        locales: ['vi-VN'],
        defaultLocale: 'vi-VN',
    },
    // images: { domains: ['api.demo.vn', 'i.ytimg.com'] },
    reactStrictMode: true,
    // rewrites: async () => [
    //     {
    //         source: '/api/:path*',
    //         destination: process.env.NEXT_PUBLIC_API_URL + ':path*',
    //     },
    // ],
    sassOptions: {
        includePaths: [path.join(__dirname, 'styles')],
    },
});
