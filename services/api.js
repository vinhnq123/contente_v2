import axios from 'axios';
import { accessToken, isLoggedIn } from '../common/auth';

const baseURL = process.env.NEXT_PUBLIC_API_URL;

const REQUEST_HEADERS = {
    'Content-Type': 'application/x-www-form-urlencoded',
    'Accept': '*/*',
    'X-Requested-With': 'XMLHttpRequest',
    'Cache-Control': 'no-store',
};

const api = axios.create({
    baseURL: baseURL,
    headers: REQUEST_HEADERS,
});

api.interceptors.response.use(
    response => response,
    error => {
        return Promise.resolve({ error });
    }
);

const auth = req => {
    const headers = { ...REQUEST_HEADERS };
    if (isLoggedIn(req)) {
        headers['XF-Api-User'] = accessToken(req);
    }
    return axios.create({
        baseURL: baseURL,
        headers,
    });
};

/* auth.interceptors.request.use(config => {
    if (isLoggedIn()) {
        config.headers['XF-Api-User'] = accessToken();
    }
    return config;
}); */

auth.interceptors.response.use(
    response => response,
    error => Promise.resolve({ error })
);

export { api, auth, REQUEST_HEADERS };
