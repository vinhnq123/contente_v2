import ModalSearch from '../Menu/ModalSearch';
import Link from 'next/link';
import { _url } from '../../../common/utils';
import { useState } from 'react';
import { useEffect } from 'react';
import { isLoggedIn } from '../../../common/auth';

export default function Menu() {
    const [mounted, setMounted] = useState();

    useEffect(() => {
        setMounted(true);
    }, []);

    return (
        <>
            <div className="outer-nav">
                <Link href="/">
                    <a className="btn-home">Trang chủ</a>
                </Link>
                <ModalSearch />
                <Link href={_url.page('categories')}>
                    <a className="mb" title='Chuyên mục'>
                        <i className="icon icon-cate"></i>
                        <span>Chuyên mục</span>
                    </a>
                </Link>
                <Link href={_url.page('community')}>
                    <a className="mb" title='Cộng đồng'>
                        <i className="icon icon-list-user"></i>
                        <span>Cộng đồng</span>
                    </a>
                </Link>
            </div>
        </>
    );
}
