import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import { useState } from 'react';
import { _url } from '../../../common/utils';
import { copyLink } from '../../../common/Support/helpers';
import { toast } from 'react-toastify';

export default function DropdownFeed({ thread }) {
    const [dropdownOpen, setOpen] = useState(false);
    const toggle = () => setOpen(!dropdownOpen);

    return (
        <>
            <Dropdown className="tool-dropdown" isOpen={dropdownOpen} toggle={toggle}>
                <DropdownToggle aria-label="mở rộng">
                    <span className="btn-menu">
                        <i className="icon icon-dotted"></i>
                    </span>
                </DropdownToggle>
                <DropdownMenu className="dropdown-menu-lg-end">
                    <DropdownItem>
                        <span>Report</span>
                    </DropdownItem>
                    <DropdownItem
                        onClick={e =>
                            copyLink(e, {
                                url: _url.article(thread),
                                callback: () => toast.success('Đã sao chép liên kết.'),
                            })
                        }
                    >
                        <span>Copy link</span>
                    </DropdownItem>
                </DropdownMenu>
            </Dropdown>
        </>
    );
}
