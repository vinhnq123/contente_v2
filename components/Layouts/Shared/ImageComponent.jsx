import { useState } from 'react';
import NextImage from 'next/image';
import { _obj, _str } from '../../../common/Support/helpers';
import { showAuthor, THUMBNAIL_DEFAULT } from '../../../common/utils';

export const RATIO_BANNER = 1529 / 229;

export default function ImageComponent(props) {
    const { src, srcDefault, onError, loadingBlur, ...rest } = props;
    const [imgError, setImgError] = useState(false);
    const [oldSrc, setOldSrc] = useState(src);

    if (oldSrc !== src) {
        setImgError(false);
        setOldSrc(src);
    }

    return imgError && onError ? (
        onError()
    ) : (
        <NextImage
            src={imgError ? srcDefault : src}
            {...rest}
            alt={_obj.get(rest, 'alt', 'image')}
            onError={() => setImgError(true)}
            placeholder={loadingBlur ? 'blur' : 'empty'}
            {...{ blurDataURL: loadingBlur ? THUMBNAIL_DEFAULT : null }}
            layout="responsive"
        />
    );
}

export function AvatarComponent(props) {
    return props?.user?.avatar_urls?.m ? (
        <ImageComponent
            src={_obj.get(props, 'user.avatar_urls.m', '')}
            alt={showAuthor(props.user)}
            onError={() => <AvatarDefault name={_obj.get(props, 'user.username', 'avatar')} {...props} />}
            {...props}
        />
    ) : (
        <AvatarDefault name={_obj.get(props, 'user.username', 'avatar')} {...props} />
    );
}

export function AvatarDefault({ name }) {
    const generateText = () => {
        const _names = name.split(' ');
        let result = '';
        for (let i = 0; i < 2; i++) {
            result += _str.of(_names[i]).nonUnicode().title().get().substring(0, 1);
        }
        return result;
    };

    return <span className={`avatar-default`}>{generateText()}</span>;
}

export function createImage(url) {
    return new Promise((resolve, reject) => {
        const image = new Image();
        image.addEventListener('load', () => resolve(image));
        image.addEventListener('error', error => reject(error));
        image.setAttribute('crossOrigin', 'anonymous');
        image.src = url;
    });
}

export async function getCroppedImg(imageSrc, pixelCrop) {
    const image = await createImage(imageSrc);
    pixelCrop.width = pixelCrop?.width || image.width;
    pixelCrop.height = pixelCrop?.height || image.width / RATIO_BANNER;
    const canvas = document.createElement('canvas');
    canvas.width = pixelCrop.width;
    canvas.height = pixelCrop.height;
    const ctx = canvas.getContext('2d');

    ctx.fillStyle = '#ffffff';
    ctx.fillRect(0, 0, canvas.width, canvas.height);
    ctx.drawImage(
        image,
        pixelCrop.x,
        pixelCrop.y,
        pixelCrop.width,
        pixelCrop.height,
        0,
        0,
        pixelCrop.width,
        pixelCrop.height
    );

    // As Base64 string
    return canvas.toDataURL('image/jpeg');

    // As a blob
    // return new Promise((resolve, reject) => {
    //     canvas.toBlob(file => {
    //         resolve(URL.createObjectURL(file));
    //     }, 'image/jpeg');
    // });
}
