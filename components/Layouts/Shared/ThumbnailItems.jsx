import { isset, _obj } from '../../../common/Support/helpers';
import { THUMBNAIL_DEFAULT, _url } from '../../../common/utils';
import ImageComponent from './ImageComponent';
import FsLightbox from 'fslightbox-react';
import { useState } from 'react';

export default function ThumbnailItems({ thread, isPost }) {
    const thumbnails = _obj.get(thread, `${isPost ? '' : 'FirstPost.'}Attachments`, []);
    const [lightboxController, setLightboxController] = useState({
        toggler: false,
        slide: 1,
    });
    const sources = thumbnails.map(thumbnail => thumbnail.direct_url);

    const openLightboxOnSlide = number => {
        if (isPost) {
            setLightboxController({
                toggler: !lightboxController.toggler,
                slide: number,
            });
        } else {
            window.location.href = _url.article(thread);
        }
    };

    const loadImageItems = () => {
        if (thumbnails.length === 1) {
            return (
                <figure className="outer-img" onClick={() => openLightboxOnSlide(1)}>
                    <ImageComponent
                        src={_obj.get(
                            thread,
                            `${isPost ? '' : 'FirstPost.'}Attachments.0.direct_url`,
                            THUMBNAIL_DEFAULT
                        )}
                        width="815"
                        height="500"
                        loadingBlur
                        srcDefault={THUMBNAIL_DEFAULT}
                    />
                </figure>
            );
        } else {
            return (
                <div className={`img-style img-style-${thumbnails.length <= 4 ? thumbnails.length : 's'}`}>
                    {thumbnails.map((thumbnail, position) => {
                        return (
                            <figure
                                className="outer-img"
                                key={thumbnail.attachment_id}
                                onClick={() => openLightboxOnSlide(position + 1)}
                            >
                                <span>
                                    <ImageComponent
                                        src={_obj.get(thumbnail, 'thumbnail_url', THUMBNAIL_DEFAULT)}
                                        alt={thread.title}
                                        width="750"
                                        height="460"
                                        loadingBlur
                                        srcDefault={THUMBNAIL_DEFAULT}
                                    />
                                </span>
                            </figure>
                        );
                    })}
                </div>
            );
        }
    };

    return (
        <>
            {loadImageItems()}
            {isPost && (
                <FsLightbox toggler={lightboxController.toggler} sources={sources} slide={lightboxController.slide} />
            )}
        </>
    );
}
