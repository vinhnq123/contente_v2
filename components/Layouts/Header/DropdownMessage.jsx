import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { sharedFetchConversationAction } from '../../../redux/actions/sharedActions';
import { _url } from '../../../common/utils';
import { empty } from '../../../common/Support/helpers';

function hasConversation(conversations) {
    if (!empty(conversations)) {
        let countUnread = 0;
        conversations.forEach(conversation => {
            conversation.is_unread && countUnread++;
        });
        return countUnread !== 0;
    }
    return false;
}

export default function DropdownMessage() {
    const [dropdownOpen, setOpen] = useState(false);
    const toggle = () => setOpen(!dropdownOpen);
    const conversations = useSelector(state => state.sharedReducers.conversations);
    const loadFinish = useSelector(state => state.sharedReducers.conversationLoadFinish);
    const dispatch = useDispatch();

    useEffect(() => {
        loadFinish || dispatch(sharedFetchConversationAction());
    }, [loadFinish]);

    return (
        <>
            <Dropdown className="outer-dropdown dropdown-mess" isOpen={dropdownOpen} toggle={toggle}>
                <DropdownToggle aria-label="Tin nhắn">
                    <span className={`btn-menu${hasConversation(conversations) ? ' active' : ''}`}>
                        <i className="icon icon-messege"></i>
                    </span>
                </DropdownToggle>
                <div className="wrapper-dropdown">
                    <DropdownMenu className="inner-dropdown text-center">
                        <DropdownItem className="head" href="#" title="">
                            Tin nhắn
                        </DropdownItem>
                        <div className="scroll">
                            {conversations.map(conversation => (
                                <DropdownItem
                                    className={`text-left${conversation.is_unread ? '' : ' text-muted'}`}
                                    href={_url.replyConversation(conversation.conversation_id)}
                                    key={conversation.conversation_id}
                                >
                                    <strong>{conversation.username}</strong> {conversation.title} <br />
                                    Cùng với: <b>{Object.values(conversation.recipients).join(', ')}</b>
                                </DropdownItem>
                            ))}
                        </div>
                        {conversations.length ? (
                            <DropdownItem className="btn-seemore" href={_url.page('conversations')} title="Xem thêm">
                                Xem thêm
                            </DropdownItem>
                        ) : (
                            <DropdownItem className="mt-3" text>
                                Không có tin nhắn
                            </DropdownItem>
                        )}
                    </DropdownMenu>
                </div>
            </Dropdown>
        </>
    );
}
