import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Dropdown, DropdownToggle, DropdownMenu, DropdownItem } from 'reactstrap';
import { _url } from '../../../common/utils';
import { empty, _obj } from '../../../common/Support/helpers';
import { sharedFetchAlertsActions } from '../../../redux/actions/sharedActions';
import threadService from '../../../services/threadService';

function hasNotification(alerts) {
    if (!empty(alerts)) {
        let countUnread = 0;
        alerts.forEach(alert => {
            if (alert.read_date === 0) {
                countUnread++;
            }
        });
        return countUnread !== 0;
    }
    return false;
}

export default function DropdownNotify() {
    const [dropdownOpen, setOpen] = useState(false);
    const toggle = () => setOpen(!dropdownOpen);
    const alerts = useSelector(state => state.sharedReducers.alerts);
    const loadFinish = useSelector(state => state.sharedReducers.alertLoadFinish);
    const dispatch = useDispatch();

    useEffect(() => {
        loadFinish || dispatch(sharedFetchAlertsActions());
    }, [loadFinish]);

    const handleRedirectToThread = post_id => {
        threadService.getDetailByPost({ path: { post_id } }).then(res => {
            if (res?.status === 200) {
                window.location.href = _url.article(res.data.thread).get();
            }
        });
    };

    return (
        <>
            <Dropdown className="outer-dropdown" isOpen={dropdownOpen} toggle={toggle}>
                <DropdownToggle aria-label="Thông báo">
                    <span className={`btn-menu${hasNotification(alerts) ? ' active' : ''}`}>
                        <i className="icon icon-notify"></i>
                    </span>
                </DropdownToggle>
                <div className="wrapper-dropdown">
                    <DropdownMenu className="inner-dropdown text-center">
                        <DropdownItem className="head" href="#" title="Thông báo">
                            THÔNG BÁO
                        </DropdownItem>
                        <div className="scroll">
                            {alerts.map(alert => (
                                <DropdownItem
                                    className={`text-left${alert.read_date === 0 ? ' fw-normal' : ' text-muted'}`}
                                    key={alert.alert_id}
                                    onClick={() => handleRedirectToThread(alert.content_id)}
                                >
                                    {alert.alert_text}
                                </DropdownItem>
                            ))}
                        </div>
                        {alerts.length ? (
                            <DropdownItem
                                className="text-left btn-seemore"
                                href={_url.page('notifications')}
                                title="Xem thêm"
                            >
                                Xem thêm
                            </DropdownItem>
                        ) : (
                            <DropdownItem className="mt-3" text>
                                Không có thông báo
                            </DropdownItem>
                        )}
                    </DropdownMenu>
                </div>
            </Dropdown>
        </>
    );
}
