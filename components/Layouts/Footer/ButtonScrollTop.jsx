import { useEffect, useState } from 'react';
import { _url } from '../../../common/utils';

const btnTop = {
    position: 'relative',
};

export default function ButtonScrollTop() {
    const [showButton, setShowButton] = useState(false);

    useEffect(() => {
        const handleScroll = () => {
            if (window.pageYOffset > 300) {
                setShowButton(true);
            } else {
                setShowButton(false);
            }
        };
        window.addEventListener('scroll', handleScroll);
        return () => {
            window.removeEventListener('scroll', handleScroll);
        };
    }, []);

    return (
        showButton && (
            <div className="lists-logo">
                <a
                    className="btn-top"
                    style={btnTop}
                    onClick={() =>
                        window.scrollTo({
                            top: 0,
                            behavior: 'smooth',
                        })
                    }
                >
                    <i className="icon icon-top"></i>
                </a>
            </div>
        )
    );
}
