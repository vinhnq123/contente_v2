import ImageComponent from '../Layouts/Shared/ImageComponent';
import { Row, Col } from 'reactstrap';

export default function Dontmiss() {
    return (
        <>
            <article className="art-vertical art-big">
                <div className="outer-thumb">
                    <a className="thumb thumb-4x3 shadow">
                        <ImageComponent src="/static/images/thumbnails/1.jpg" alt="thumb" width="630" height="370" />
                    </a>
                    <a className="link-cat" href="#" title="">
                        {"Shopper's Opinion"}
                    </a>
                    <div className="bar">
                        <div className="outer-view">
                            <i className="icon icon-view" />
                            <strong>5000</strong>
                        </div>
                        <a className="btn-view" href="#" title="">
                            Xem ngay
                        </a>
                    </div>
                </div>
                <header>
                    <h3 className="pt-3">
                        <a href="#" title="">
                            Ồ wow story: Samsung x Laneige - Khi đồng điệu tạo nên đẳng cấp
                        </a>
                    </h3>
                    <span className="date">20/10/2021</span>
                </header>
            </article>
            <article className="art-horizontal art-right">
                <div className="outer-thumb">
                    <a className="thumb thumb-3x2 shadow">
                        <ImageComponent src="/static/images/thumbnails/2.jpg" alt="thumb" width="142" height="88" />
                    </a>
                </div>
                <header>
                    <h4>
                        <a href="#" title="">
                            Khẩu trang thông minh Xiaomi Purely
                        </a>
                    </h4>
                </header>
            </article>
            <article className="art-horizontal art-right">
                <div className="outer-thumb">
                    <a className="thumb thumb-3x2 shadow">
                        <ImageComponent src="/static/images/thumbnails/2.jpg" alt="thumb" width="142" height="88" />
                    </a>
                </div>
                <header>
                    <h4>
                        <a href="#" title="">
                            Khẩu trang thông minh Xiaomi Purely
                        </a>
                    </h4>
                </header>
            </article>
            <article className="art-horizontal art-right">
                <div className="outer-thumb">
                    <a className="thumb thumb-3x2 shadow">
                        <ImageComponent src="/static/images/thumbnails/2.jpg" alt="thumb" width="142" height="88" />
                    </a>
                </div>
                <header>
                    <h4>
                        <a href="#" title="">
                            Khẩu trang thông minh Xiaomi Purely
                        </a>
                    </h4>
                </header>
            </article>
            <article className="art-horizontal art-right">
                <div className="outer-thumb">
                    <a className="thumb thumb-3x2 shadow">
                        <ImageComponent src="/static/images/thumbnails/2.jpg" alt="thumb" width="142" height="88" />
                    </a>
                </div>
                <header>
                    <h4>
                        <a href="#" title="">
                            Khẩu trang thông minh Xiaomi Purely
                        </a>
                    </h4>
                </header>
            </article>
        </>
    );
}
