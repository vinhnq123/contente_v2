import ImageComponent from '../Layouts/Shared/ImageComponent';
import { Splide, SplideSlide } from '@splidejs/react-splide';
import '@splidejs/splide/dist/css/splide.min.css';
import { THUMBNAIL_DEFAULT } from '../../common/utils';

export default function HighlightSlide() {
    return (
        <>
            <Splide
                className="highlight-slide-cate"
                options={{
                    breakpoints: {
                        5000: {
                            destroy: true,
                        },
                        991: {
                            rewind: true,
                            perPage: 1,
                            height: 'auto',
                            gap: '1rem',
                            padding: '40px',
                            type: 'loop',
                        },
                    },
                }}
            >
                <SplideSlide>
                    <article className="art-vertical">
                        <div className="outer-thumb">
                            <a className="thumb thumb-4x3 shadow">
                                <ImageComponent
                                    src="/static/images/thumbnails/2.jpg"
                                    alt="thumb"
                                    width="304"
                                    height="228"
                                    loadingBlur
                                    srcDefault={THUMBNAIL_DEFAULT}
                                />
                            </a>
                            <a className="link-style" href="/category" title="">
                                Ồ Wow Story
                            </a>
                            <i className="icon icon-play" />
                        </div>
                        <header>
                            <h2>
                                <a href="/article" title="">
                                    3 Mẹo nhỏ để cân đối bữa ăn trong ngày giãn cách
                                </a>
                            </h2>
                            <span className="date">20/10/2021</span>
                            <div className="author">
                                <a href="#" title="">
                                    <ImageComponent
                                        className="avatar"
                                        src="/static/images/thumbnails/3.jpg"
                                        alt="thumb"
                                        width="32"
                                        height="32"
                                    />
                                    <span>
                                        <strong>Nguyễn Anh Khánh</strong>
                                        V.I.P
                                    </span>
                                </a>
                            </div>
                            <div className="bar">
                                <div className="outer-view">
                                    <i className="icon icon-view-b" />
                                    <strong>5000</strong>
                                </div>
                                <a className="btn-view" href="/article" title="">
                                    Xem ngay
                                </a>
                            </div>
                        </header>
                    </article>
                </SplideSlide>
                <SplideSlide>
                    <article className="art-vertical">
                        <div className="outer-thumb">
                            <a className="thumb thumb-4x3 shadow">
                                <ImageComponent
                                    src="/static/images/thumbnails/4.jpg"
                                    alt="thumb"
                                    width="304"
                                    height="228"
                                    loadingBlur
                                    srcDefault={THUMBNAIL_DEFAULT}
                                />
                            </a>
                            <a className="link-style" href="#" title="">
                                Ồ Wow Story
                            </a>
                            <i className="icon icon-play" />
                        </div>
                        <header>
                            <h2>
                                <a href="#" title="">
                                    Số ca nhiễm vovid tăng nhanh trong tháng 8
                                </a>
                            </h2>
                            <span className="date">20/10/2021</span>
                            <div className="author">
                                <a href="#" title="">
                                    <ImageComponent
                                        className="avatar"
                                        src="/static/images/thumbnails/5.jpg"
                                        alt="thumb"
                                        width="32"
                                        height="32"
                                    />
                                    <span>
                                        <strong>Nguyễn Minh Trí</strong>
                                        V.I.P
                                    </span>
                                </a>
                            </div>
                            <div className="bar">
                                <div className="outer-view">
                                    <i className="icon icon-view-b" />
                                    <strong>2600</strong>
                                </div>
                                <a className="btn-view" href="#" title="">
                                    Xem ngay
                                </a>
                            </div>
                        </header>
                    </article>
                </SplideSlide>
                <SplideSlide>
                    <article className="art-vertical">
                        <div className="outer-thumb">
                            <a className="thumb thumb-4x3 shadow">
                                <ImageComponent
                                    src="/static/images/thumbnails/6.jpg"
                                    alt="thumb"
                                    width="304"
                                    height="228"
                                    loadingBlur
                                    srcDefault={THUMBNAIL_DEFAULT}
                                />
                            </a>
                            <a className="link-style" href="#" title="">
                                Ồ Wow Story
                            </a>
                            <i className="icon icon-play" />
                        </div>
                        <header>
                            <h2>
                                <a href="#" title="">
                                    Bà Nguyễn Phương Hằng live vào lúc 5h hàng ngày
                                </a>
                            </h2>
                            <span className="date">20/10/2021</span>
                            <div className="author">
                                <a href="#" title="">
                                    <ImageComponent
                                        className="avatar"
                                        src="/static/images/thumbnails/7.jpg"
                                        alt="thumb"
                                        width="32"
                                        height="32"
                                    />
                                    <span>
                                        <strong>Nguyễn Anh Khánh</strong>
                                        V.I.P
                                    </span>
                                </a>
                            </div>
                            <div className="bar">
                                <div className="outer-view">
                                    <i className="icon icon-view-b" />
                                    <strong>1900</strong>
                                </div>
                                <a className="btn-view" href="#" title="">
                                    Xem ngay
                                </a>
                            </div>
                        </header>
                    </article>
                </SplideSlide>
                <SplideSlide>
                    <article className="art-vertical">
                        <div className="outer-thumb">
                            <a className="thumb thumb-4x3 shadow">
                                <ImageComponent
                                    src="/static/images/thumbnails/7.jpg"
                                    alt="thumb"
                                    width="304"
                                    height="228"
                                    loadingBlur
                                    srcDefault={THUMBNAIL_DEFAULT}
                                />
                            </a>
                            <a className="link-style" href="#" title="">
                                Ồ Wow Story
                            </a>
                            <a className="link-cat" href="#" title="">
                                {`Shopper's Opinion`}
                            </a>
                        </div>
                        <header>
                            <h2>
                                <a href="#" title="">
                                    Trà sữa O long mở chia nhánh mới ở Thử Đức
                                </a>
                            </h2>
                            <span className="date">21/12/2021</span>
                            <div className="author">
                                <a href="#" title="">
                                    <ImageComponent
                                        className="avatar"
                                        src="/static/images/thumbnails/8.jpg"
                                        alt="thumb"
                                        width="32"
                                        height="32"
                                    />
                                    <span>
                                        <strong>Hà Mỹ Duyên</strong>
                                        V.I.P
                                    </span>
                                </a>
                            </div>
                            <div className="bar">
                                <div className="outer-view">
                                    <i className="icon icon-view-b" />
                                    <strong>8700</strong>
                                </div>
                                <a className="btn-view" href="#" title="">
                                    Xem ngay
                                </a>
                            </div>
                        </header>
                    </article>
                </SplideSlide>
            </Splide>
        </>
    );
}
