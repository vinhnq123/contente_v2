import { useEffect, useState } from 'react';
import { AvatarComponent } from '../Layouts/Shared/ImageComponent';
import { showAuthor, _url } from '../../common/utils';
import nodeService from '../../services/nodeService';
import { _obj } from '../../common/Support/helpers';

export default function Mods({ node }) {
    const [users, setUsers] = useState([]);

    useEffect(() => {
        (() => {
            nodeService.getListMod({ path: { node_id: node.node_name } }).then(res => {
                if (res?.status === 200) {
                    setUsers(_obj.get(res, 'data.users', []));
                }
            });
        })();
    }, []);

    return (
        <>
            <div className="block-bar">
                <h2>Mods</h2>
                <div className="inner-bar">
                    {users.map(user => (
                        <article className="art-admin" key={user.user_id}>
                            <a className="thumb" href={_url.user(user)} title={showAuthor(user)}>
                                <AvatarComponent width="90" height="90" loadingBlur user={user} />
                            </a>
                            <header>
                                <a className="name name-blue" href={_url.user(user)} title={showAuthor(user)}>
                                    {showAuthor(user)}
                                </a>
                            </header>
                        </article>
                    ))}
                </div>
            </div>
        </>
    );
}
