import { useCallback, useEffect, useState } from 'react';
import { _obj } from '../../common/Support/helpers';
import { INIT_PAGINATION, _url } from '../../common/utils';
import threadService from '../../services/threadService';
import ArticleItem from '../Layouts/Shared/ArticleItem';
import { Swiper, SwiperSlide } from 'swiper/react';
import SwiperCore, { Autoplay } from 'swiper';
import 'swiper/css';

export default function List(props) {
    const [threads, setThreads] = useState(props.threads);
    const [pagination, setPagination] = useState(props.pagination);
    const [loadFinish, setLoadFinish] = useState(true);
    const [type, setType] = useState('newest');

    useEffect(() => {
        loadFinish || fetchThreads(1);
    }, [loadFinish]);

    const fetchThreads = useCallback(
        page => {
            threadService.getListByNode({ path: { node_id: props.node.node_id }, query: { type, page } }).then(res => {
                if (res?.status === 200) {
                    const data = _obj.get(res, 'data.threads', []);
                    setThreads(current => (page === 1 ? data : current.concat(data)));
                    setPagination(_obj.get(res, 'data.pagination', INIT_PAGINATION));
                }
                setLoadFinish(true);
            });
        },
        [pagination.current_page, type]
    );

    const handleChangeType = type => {
        setType(type);
        setLoadFinish(false);
    };

    return (
        <>
            <div className="box-find box-Filter">
                <Swiper slidesPerView={'auto'} spaceBetween={0} className="mySwiper swiper-filter">
                    <SwiperSlide>
                        <span
                            title="Mới nhất"
                            className={type === 'newest' ? 'active' : ''}
                            onClick={() => handleChangeType('newest')}
                        >
                            Mới nhất
                        </span>
                    </SwiperSlide>
                    <SwiperSlide>
                        <span
                            title="Nổi bật"
                            className={type === 'top' ? 'active' : ''}
                            onClick={() => handleChangeType('top')}
                        >
                            Nổi bật
                        </span>
                    </SwiperSlide>
                    <SwiperSlide>
                        <span
                            title="Được quan tâm"
                            className={type === 'hot' ? 'active' : ''}
                            onClick={() => handleChangeType('hot')}
                        >
                            Quan tâm
                        </span>
                    </SwiperSlide>
                </Swiper>
                <a className="btn-link-1" href={_url.page('categories')}>
                    <i className="icon icon-dotted"></i>
                </a>
            </div>
            {threads.map(thread => (
                <ArticleItem thread={thread} key={thread.thread_id} />
            ))}
            <div className="d-flex justify-content-center">
                <button
                    className="btn-style btn-big mt-2 mb-5"
                    disabled={pagination.current_page >= pagination.last_page}
                    onClick={() => {
                        if (pagination.current_page < pagination.last_page) {
                            fetchThreads(pagination.current_page + 1);
                        }
                    }}
                >
                    Xem thêm
                </button>
            </div>
        </>
    );
}
