import ImageComponent from '../Layouts/Shared/ImageComponent';
import { Row, Col } from 'reactstrap';

export default function Highlight() {
    return (
        <>
            <Row className="row-1">
                <Col className="col-md-3">
                    <article className="art-vertical">
                        <div className="outer-thumb">
                            <a className="thumb thumb-4x3 shadow">
                                <ImageComponent
                                    src="/static/images/thumbnails/2.jpg"
                                    alt="thumb"
                                    width="630"
                                    height="370"
                                />
                            </a>
                            <a className="link-style" href="#" title="">
                                Ồ Wow Story
                            </a>
                            <i className="icon icon-play" />
                        </div>
                        <header>
                            <h2>
                                <a href="#" title="">
                                    3 Mẹo nhỏ để cân đối bữa ăn trong ngày giãn cách
                                </a>
                            </h2>
                            <span className="date">20/10/2021</span>
                            <div className="author">
                                <a href="#" title="">
                                    <ImageComponent
                                        className="avatar"
                                        src="/static/images/thumbnails/3.jpg"
                                        alt="thumb"
                                        width="32"
                                        height="32"
                                    />
                                    <span>
                                        <strong>Nguyễn Anh Khánh</strong>
                                        V.I.P
                                    </span>
                                </a>
                            </div>
                            <div className="bar">
                                <div className="outer-view">
                                    <i className="icon icon-view-b" />
                                    <strong>5000</strong>
                                </div>
                                <a className="btn-view" href="#" title="">
                                    Xem ngay
                                </a>
                            </div>
                        </header>
                    </article>
                </Col>
                <Col className="col-md-3">
                    <article className="art-vertical">
                        <div className="outer-thumb">
                            <a className="thumb thumb-4x3 shadow">
                                <ImageComponent
                                    src="/static/images/thumbnails/4.jpg"
                                    alt="thumb"
                                    width="630"
                                    height="370"
                                />
                            </a>

                            <a className="link-style" href="#" title="">
                                Ồ Wow Story
                            </a>
                        </div>
                        <header>
                            <h2>
                                <a href="#" title="">
                                    3 Mẹo nhỏ để cân đối bữa ăn trong ngày giãn cách
                                </a>
                            </h2>
                            <span className="date">20/10/2021</span>
                            <div className="author">
                                <a href="#" title="">
                                    <ImageComponent
                                        className="avatar"
                                        src="/static/images/thumbnails/5.jpg"
                                        alt="thumb"
                                        width="32"
                                        height="32"
                                    />
                                    <span>
                                        <strong>Nguyễn Minh Trí</strong>
                                        New User
                                    </span>
                                </a>
                            </div>
                            <div className="bar">
                                <div className="outer-view">
                                    <i className="icon icon-view-b" />
                                    <strong>5000</strong>
                                </div>
                                <a className="btn-view" href="#" title="">
                                    Xem ngay
                                </a>
                            </div>
                        </header>
                    </article>
                </Col>
                <Col className="col-md-3">
                    <article className="art-vertical">
                        <div className="outer-thumb">
                            <a className="thumb thumb-4x3 shadow">
                                <ImageComponent
                                    src="/static/images/thumbnails/6.jpg"
                                    alt="thumb"
                                    width="630"
                                    height="370"
                                />
                            </a>

                            <a className="link-style" href="#" title="">
                                Ồ Wow Story
                            </a>
                        </div>
                        <header>
                            <h2>
                                <a href="#" title="">
                                    3 Mẹo nhỏ để cân đối bữa ăn trong ngày giãn cách
                                </a>
                            </h2>
                            <span className="date">20/10/2021</span>
                            <div className="author">
                                <a href="#" title="">
                                    <ImageComponent
                                        className="avatar"
                                        src="/static/images/thumbnails/7.jpg"
                                        alt="thumb"
                                        width="32"
                                        height="32"
                                    />
                                    <span>
                                        <strong>Nguyễn Minh Trí</strong>
                                        New User
                                    </span>
                                </a>
                            </div>
                            <div className="bar">
                                <div className="outer-view">
                                    <i className="icon icon-view-b" />
                                    <strong>5000</strong>
                                </div>
                                <a className="btn-view" href="#" title="">
                                    Xem ngay
                                </a>
                            </div>
                        </header>
                    </article>
                </Col>
                <Col className="col-md-3">
                    <article className="art-vertical">
                        <div className="outer-thumb">
                            <a className="thumb thumb-4x3 shadow">
                                <ImageComponent
                                    src="/static/images/thumbnails/8.jpg"
                                    alt="thumb"
                                    width="630"
                                    height="370"
                                />
                            </a>
                            <a className="link-cat" href="#" title="">
                                {"Shopper's Opinion"}
                            </a>
                            <a className="link-style" href="#" title="">
                                Ồ Wow Story
                            </a>
                        </div>
                        <header>
                            <h2>
                                <a href="#" title="">
                                    3 Mẹo nhỏ để cân đối bữa ăn trong ngày giãn cách
                                </a>
                            </h2>
                            <span className="date">20/10/2021</span>
                            <div className="author">
                                <a href="#" title="">
                                    <ImageComponent
                                        className="avatar"
                                        src="/static/images/thumbnails/9.jpg"
                                        alt="thumb"
                                        width="32"
                                        height="32"
                                    />
                                    <span>
                                        <strong>Nguyễn Minh Trí</strong>
                                        New User
                                    </span>
                                </a>
                            </div>
                            <div className="bar">
                                <div className="outer-view">
                                    <i className="icon icon-view-b" />
                                    <strong>5000</strong>
                                </div>
                                <a className="btn-view" href="#" title="">
                                    Xem ngay
                                </a>
                            </div>
                        </header>
                    </article>
                </Col>
            </Row>
        </>
    );
}
