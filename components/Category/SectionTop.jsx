import { useState } from 'react';
import { useDispatch } from 'react-redux';
import { notLoggedIn } from '../../common/auth';
import { THUMBNAIL_DEFAULT, toastPromiseOptions, trans, _url } from '../../common/utils';
import nodeService from '../../services/nodeService';
import ImageComponent from '../Layouts/Shared/ImageComponent';
import { toast } from 'react-toastify';
import { sharedTogglePopupRequireLoginAction } from '../../redux/actions/sharedActions';
import { _obj } from '../../common/Support/helpers';

export default function SectionTop({ node, listNodesWatched }) {
    const [followed, setFollowed] = useState(listNodesWatched.find(item => item.node_id === node.node_id) ? 1 : 0);
    const [receivedAlert, setReceivedAlert] = useState(node.send_alert);
    const dispatch = useDispatch();

    const handleFollowCategory = () => {
        if (notLoggedIn()) {
            dispatch(
                sharedTogglePopupRequireLoginAction({
                    show: true,
                    redirectTo: _url.category(node),
                })
            );
        } else {
            toast
                .promise(
                    nodeService.follow({
                        path: { node_id: node.node_id },
                        query: { stop: followed, send_alert: 1, send_email: 0 },
                    }),
                    toastPromiseOptions({
                        messageSuccess: followed ? trans('Unfollowed topic') : trans('Followed topic'),
                        messageError: trans('You cannot follow this topic'),
                    })
                )
                .then(res => {
                    if (res?.status === 200) {
                        setFollowed(!followed);
                        setReceivedAlert(!followed);
                    }
                });
        }
    };

    const handleReceivedAlert = () => {
        if (notLoggedIn()) {
            dispatch(
                sharedTogglePopupRequireLoginAction({
                    show: true,
                    redirectTo: _url.category(node),
                })
            );
        } else {
            toast
                .promise(
                    nodeService.follow({
                        path: { node_id: node.node_id },
                        query: { stop: 0, send_alert: receivedAlert ? 0 : 1, send_email: 0 },
                    }),
                    toastPromiseOptions({
                        success: receivedAlert ? trans('Unreceived alert') : trans('Received alert'),
                    })
                )
                .then(res => {
                    if (res?.status === 200) {
                        setReceivedAlert(!receivedAlert);
                        setFollowed(1);
                    }
                });
        }
    };

    return (
        <>
            <section className="section mt-0">
                <div className="block-cate">
                    <div className="img-cover">
                        <ImageComponent
                            src={_obj.get(
                                node,
                                'type_data.last_thread_title_attachments.0.direct_url',
                                THUMBNAIL_DEFAULT
                            )}
                            alt="thumb"
                            width="1300"
                            height="300"
                            loadingBlur
                            srcDefault={THUMBNAIL_DEFAULT}
                            className="img-component-cover"
                        />
                    </div>
                    <div className="des">
                        <div className="container">
                            <h1>{node.title}</h1>
                            <div className="btn-follow">
                                <span onClick={handleFollowCategory}>{followed ? 'Bỏ theo dõi' : 'Theo dõi'}</span>
                                <i
                                    className={`icon icon-follow${receivedAlert ? ' unfollow' : ''}`}
                                    title={receivedAlert ? 'Tắt thông báo' : 'Bật thông báo'}
                                    onClick={handleReceivedAlert}
                                />
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </>
    );
}
