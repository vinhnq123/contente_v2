import { useState } from 'react';
import { AvatarComponent } from '../Layouts/Shared/ImageComponent';
import { showAuthor, _url } from '../../common/utils';
import { useEffect } from 'react';
import memberService from '../../services/memberService';
import { _obj } from '../../common/Support/helpers';

export default function Admins({ node }) {
    const [users, setUsers] = useState([]);

    useEffect(() => {
        (() => {
            memberService.getListAdmins().then(res => {
                if (res?.status === 200) {
                    setUsers(_obj.get(res, 'data.users', []));
                }
            });
        })();
    }, []);

    return (
        <>
            <div className="block-bar">
                <h2>Admins</h2>
                <div className="inner-bar">
                    {users.map(user => (
                        <article className="art-admin" key={user.user_id}>
                            <a className="thumb" href={_url.user(user)} title={showAuthor(user)}>
                                <AvatarComponent width="48" height="48" loadingBlur user={user} />
                            </a>
                            <header>
                                <a className="name" href={_url.user(user)} title={showAuthor(user)}>
                                    {showAuthor(user)}
                                </a>
                            </header>
                        </article>
                    ))}
                </div>
            </div>
        </>
    );
}
