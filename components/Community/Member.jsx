import { useState } from 'react';
import { useDispatch } from 'react-redux';
import { toast } from 'react-toastify';
import { notLoggedIn } from '../../common/auth';
import { _obj } from '../../common/Support/helpers';
import { showAuthor, timeAgo, toastPromiseOptions, trans, _url } from '../../common/utils';
import { sharedTogglePopupRequireLoginAction } from '../../redux/actions/sharedActions';
import memberService from '../../services/memberService';
import { AvatarComponent } from '../Layouts/Shared/ImageComponent';

export default function Member({ member }) {
    const [followed, setFollowed] = useState(member.is_followed);
    const dispatch = useDispatch();

    const handleFollow = async () => {
        if (notLoggedIn()) {
            dispatch(
                sharedTogglePopupRequireLoginAction({
                    show: true,
                    redirectTo: _url.page('community'),
                })
            );
        } else {
            toast
                .promise(
                    memberService.follow({ path: member }),
                    toastPromiseOptions({
                        messageSuccess: followed ? trans('Unfollowed') : trans('Followed'),
                        messageError: followed ? trans('Cannot unfollow') : trans('Cannot follow'),
                    })
                )
                .then(res => {
                    if (res?.status === 200) {
                        setFollowed(!followed);
                    }
                });
        }
    };

    return (
        <>
            <article className="art-user-horizontal" key={member.user_id}>
                <a className="avatar" href={_url.user(member)} title={showAuthor(member)}>
                    <AvatarComponent width="90" height="90" loadingBlur user={member} />
                </a>
                <div className="des">
                    <span className="name">
                        {showAuthor(member)} <i>{timeAgo(member.last_activity)}</i>
                    </span>
                    <p>
                        <strong>{member.message_count} bài viết</strong>
                    </p>
                </div>
                <button type="button" className="btn-style-1" onClick={handleFollow}>
                    <span>{followed ? 'Bỏ theo dõi' : 'Theo dõi'}</span>
                </button>
            </article>
        </>
    );
}
