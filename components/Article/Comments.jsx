import { useEffect, useRef } from 'react';
import { _obj } from '../../common/Support/helpers';
import { _url } from '../../common/utils';
import CommentItem from './CommentItem';
import TextareaAutosize from 'react-textarea-autosize';
import { useDispatch, useSelector } from 'react-redux';
import {
    articleFetchCommentsAction,
    articlePostCommentAction,
    articleSetCommentsAction,
} from '../../redux/actions/articleActions';
import { notLoggedIn } from '../../common/auth';
import { sharedTogglePopupRequireLoginAction } from '../../redux/actions/sharedActions';

export default function Comments(props) {
    const comments = useSelector(state => state.articleReducers.comments);
    const pagination = useSelector(state => state.articleReducers.pagination);
    const loadFinish = useSelector(state => state.articleReducers.loadFinish);
    const inputCommentRef = useRef();
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(
            articleSetCommentsAction({ comments: props.comments, pagination: props.pagination, thread: props.thread })
        );
    }, []);

    useEffect(() => {
        loadFinish ||
            dispatch(
                articleFetchCommentsAction({
                    path: { thread_id: props.threadId },
                    query: { with_posts: true, page: pagination.current_page },
                })
            );
    }, [loadFinish]);

    const handlePostComment = async () => {
        if (notLoggedIn()) {
            dispatch(
                sharedTogglePopupRequireLoginAction({
                    show: true,
                    redirectTo: _url.article({ thread_id: props.threadId }).get(),
                })
            );
        } else {
            dispatch(
                articlePostCommentAction({
                    query: {
                        thread_id: props.threadId,
                        message: inputCommentRef.current.value,
                        message_json: inputCommentRef.current.value,
                    },
                })
            );
            inputCommentRef.current.value = '';
        }
    };

    const loadMore = () => {
        const { current_page, last_page } = pagination;
        if (current_page < last_page) {
            dispatch(
                articleFetchCommentsAction({
                    path: { thread_id: props.threadId },
                    query: { with_posts: true, page: pagination.current_page + 1 },
                })
            );
        }
    };

    return (
        <>
            <div className="block-comment" id="comment">
                <div className="block-binhluan mb-5">
                    <div className="box-txt" id="txt-comments">
                        <TextareaAutosize id="txt-comment" ref={inputCommentRef} placeholder="Nhập bình luận"/>
                        <button type="button" className="btn-style-1 btn-comment" onClick={handlePostComment}>
                            <span>
                                Gửi <i>bình luận</i>
                            </span>
                        </button>
                    </div>
                    {loadFinish &&
                        comments.map(
                            comment =>
                                comment.is_first_post || (
                                    <div className="box-comment" key={comment.post_id}>
                                        <CommentItem
                                            comment={comment}
                                            threadId={props.threadId}
                                            replyTo={comment.post_id}
                                        />
                                        {_obj.get(comment, 'Reply', []).map(reply => (
                                            <div className="box-comment" key={reply.post_id}>
                                                <CommentItem
                                                    comment={reply}
                                                    replyTo={comment.post_id}
                                                    threadId={props.threadId}
                                                />
                                            </div>
                                        ))}
                                    </div>
                                )
                        )}
                    {comments.length <= 1 && <span className="box-no-cm">Bạn là người bình luận đầu tiên !</span>}
                    <button
                        className="btn-style w-100 mt-5"
                        type="button"
                        disabled={pagination.current_page >= pagination.last_page}
                        onClick={loadMore}
                    >
                        Xem thêm bình luận
                    </button>
                </div>
            </div>
        </>
    );
}
