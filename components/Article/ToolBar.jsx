import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { notLoggedIn } from '../../common/auth';
import { _url } from '../../common/utils';
import { articleSetCommentCountAction } from '../../redux/actions/articleActions';
import { sharedTogglePopupRequireLoginAction } from '../../redux/actions/sharedActions';
import postService from '../../services/postService';
import DropdownComment from '../Layouts/Shared/DropdownComment';

export default function ToolBar({ thread }) {
    const [liked, setLiked] = useState(thread.FirstPost.is_reacted_to);
    const [likeCount, setLikeCount] = useState(thread.FirstPost.reaction_score);
    const commentCount = useSelector(state => state.articleReducers.commentCount);
    const dispatch = useDispatch();

    useEffect(() => {
        dispatch(articleSetCommentCountAction(thread.reply_count));
    }, []);

    const handleLike = () => {
        if (notLoggedIn()) {
            dispatch(
                sharedTogglePopupRequireLoginAction({
                    show: true,
                    redirectTo: _url.article(thread),
                })
            );
        } else {
            postService.like({ path: { post_id: thread.FirstPost.post_id }, query: { reaction_id: 1 } }).then(res => {
                if (res?.status === 200) {
                    setLiked(!liked);
                    setLikeCount(current => (liked ? --current : ++current));
                }
            });
        }
    };

    return (
        <div className="tool-cm">
            <div className="like">
                {likeCount} <span>Thích</span>
            </div>
            <div className="commnent">
                {commentCount} <span>Bình luận</span>
            </div>
            <div className="align-right">
                <span className="btn-like" onClick={handleLike}>
                    <i className={`icon icon-heart ${liked ? 'active' : ''}`} />
                    {liked ? 'Bỏ thích' : 'Thích'}
                </span>
                <DropdownComment />
            </div>
        </div>
    );
}
