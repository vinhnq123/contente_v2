import { useEffect } from 'react';
import { useState } from 'react';
import { _obj } from '../../common/Support/helpers';
import { _url } from '../../common/utils';
import threadService from '../../services/threadService';

export default function RelatedArticles({ thread }) {
    const [threads, setThreads] = useState([]);

    useEffect(() => {
        (() => {
            threadService.getListByNode({ path: { node_id: thread.node_id }, query: { limit: 6 } }).then(res => {
                if (res?.status === 200) {
                    let threads = _obj.get(res, 'data.threads', []).filter(item => item.thread_id !== thread.thread_id);
                    if (threads && threads.length > 5) {
                        threads.length = threads.length - 1;
                    }
                    setThreads(threads);
                }
            });
        })();
    }, []);

    return (
        <>
            <div className="block-related">
                <div className="title-1">BÀI TƯƠNG TỰ</div>
                <ul className="list-dot">
                    {threads.map(thread => (
                        <li key={thread.thread_id}>
                            <a href={_url.article(thread)} title={thread.title}>
                                {thread.title}
                            </a>
                        </li>
                    ))}
                </ul>
            </div>
        </>
    );
}
