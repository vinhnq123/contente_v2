import { useState } from 'react';
import { useDispatch } from 'react-redux';
import { FacebookShareButton } from 'react-share';
import { toast } from 'react-toastify';
import { notLoggedIn } from '../../common/auth';
import { toastPromiseOptions, _url } from '../../common/utils';
import { sharedTogglePopupRequireLoginAction } from '../../redux/actions/sharedActions';
import postService from '../../services/postService';

export default function ActionBar({ thread }) {
    const [bookmarked, setBookmarked] = useState(thread.FirstPost.is_bookmarked_to);
    const dispatch = useDispatch();

    const handleBookmark = () => {
        if (notLoggedIn()) {
            dispatch(
                sharedTogglePopupRequireLoginAction({
                    show: true,
                    redirectTo: window.location.href,
                })
            );
        } else {
            toast
                .promise(
                    postService.bookmark({ path: { post_id: thread.FirstPost.post_id } }, bookmarked),
                    toastPromiseOptions({
                        messageSuccess: bookmarked ? 'Đã bỏ lưu bài viết' : 'Đã lưu bài viết',
                    })
                )
                .then(res => {
                    if (res?.status === 200) {
                        setBookmarked(!bookmarked);
                    }
                });
        }
    };

    return (
        <ul className="list-tool justify-content-start mb-3">
            <li>
                <a className="btn-cm" href="#comment">
                    <i className="icon icon-binhluan"></i>
                    <span> Bình luận</span>
                </a>
            </li>
            <li>
                <FacebookShareButton url={_url.article(thread).get()} hashtag="#maybe">
                    <i className="icon icon-chiase"></i>
                    <span> Chia sẻ</span>
                </FacebookShareButton>
            </li>
            <li onClick={handleBookmark}>
                <i className={bookmarked ? 'icon icon-save active' : 'icon icon-save'} />
                <span>Lưu tin</span>
            </li>
        </ul>
    );
}
