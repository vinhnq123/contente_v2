import { useRef, useState } from 'react';
import { copyLink, serialize, _obj } from '../../common/Support/helpers';
import { showAuthor, timeAgo, _url } from '../../common/utils';
import { notLoggedIn } from '../../common/auth';
import { AvatarComponent } from '../Layouts/Shared/ImageComponent';
import ModalReport from '../Layouts/Shared/ModalReport';
import { useDispatch } from 'react-redux';
import { sharedTogglePopupRequireLoginAction } from '../../redux/actions/sharedActions';
import { articlePostCommentAction } from '../../redux/actions/articleActions';
import postService from '../../services/postService';
import { toast } from 'react-toastify';

export default function CommentItem({ comment, replyTo, threadId }) {
    const [toggleReply, setToggleReply] = useState(false);
    const COMMENT_CONTENT = _obj.get(comment, 'message_parsed', '').replace(/^\<blockquote(.?)*\<\/blockquote\>/, '');
    const [liked, setLiked] = useState(comment.is_reacted_to);
    const [likeCount, setLikeCount] = useState(comment.reaction_score);
    const inputReplyRef = useRef();
    const dispatch = useDispatch();

    const handlePostComment = () => {
        if (notLoggedIn()) {
            dispatch(
                sharedTogglePopupRequireLoginAction({
                    show: true,
                    redirectTo: _url.article({ thread_id: threadId }).get(),
                })
            );
        } else {
            dispatch(
                articlePostCommentAction({
                    query: {
                        thread_id: threadId,
                        message: inputReplyRef.current.value,
                        message_json: inputReplyRef.current.value,
                        parent_post_id: replyTo,
                    },
                })
            );
            inputReplyRef.current.value = '';
            setToggleReply(!toggleReply);
        }
    };

    const handleLike = () => {
        if (notLoggedIn()) {
            dispatch(
                sharedTogglePopupRequireLoginAction({
                    show: true,
                    redirectTo: _url.article({ thread_id: threadId }).get(),
                })
            );
        } else {
            postService.like({ path: { post_id: comment.post_id }, query: { reaction_id: 1 } }).then(res => {
                if (res?.status === 200) {
                    setLiked(!liked);
                    setLikeCount(current => (liked ? --current : ++current));
                }
            });
        }
    };

    return (
        <>
            <div className="inner-comment">
                <a className="thumb-avatar" href="#">
                    <AvatarComponent width="50" height="50" user={comment?.User} />
                </a>
                <div className="right-des">
                    <div className="comment-name">
                        <a href={_url.user(comment.User)} title={showAuthor(comment.User)}>
                            {showAuthor(comment.User)}
                        </a>
                        <span className="time">{timeAgo(comment.post_date)}</span>
                    </div>
                    <div
                        dangerouslySetInnerHTML={{
                            __html: COMMENT_CONTENT,
                        }}
                    />
                    <ul className="list-tool list-tool-left no-icon">
                        <li onClick={handleLike} className={liked ? 'outer-like active' : 'outer-like'}>
                            {/* <i className={liked ? 'icon icon-like active' : 'icon icon-like'} /> */}
                            <span className="space">{likeCount > 0 && likeCount} Thích</span>
                        </li>
                        <li onClick={() => setToggleReply(!toggleReply)}>
                            <i className="icon icon-binhluan"></i>
                            <span>{toggleReply ? 'Huỷ' : 'Trả lời'}</span>
                        </li>
                        <li
                            onClick={e =>
                                copyLink(e, {
                                    url: _url
                                        .article({ thread_id: threadId })
                                        .append(serialize({ comment: replyTo }) + '#txt-comment')
                                        .get(),
                                    callback: () => toast.success('Đã sao chép liên kết.'),
                                })
                            }
                        >
                            <i className="icon icon-chiase"></i>
                            <span>Chia sẻ</span>
                        </li>
                        <li>
                            <ModalReport />
                        </li>
                    </ul>
                    <div className={toggleReply ? 'box-txt' : 'd-none'} id={`txt-replies-${comment.post_id}`}>
                        <textarea id={`txt-reply-${comment.post_id}`} ref={inputReplyRef} />
                        <button type="button" className="btn-style-1 btn-comment" onClick={handlePostComment}>
                            <span>Trả lời</span>
                        </button>
                    </div>
                </div>
            </div>
        </>
    );
}
