import { useState } from 'react';
import { useDispatch } from 'react-redux';
import { toast } from 'react-toastify';
import { notLoggedIn } from '../../common/auth';
import { _obj } from '../../common/Support/helpers';
import { showAuthor, toastPromiseOptions, trans, _url } from '../../common/utils';
import { sharedTogglePopupRequireLoginAction } from '../../redux/actions/sharedActions';
import memberService from '../../services/memberService';
import { AvatarComponent } from '../Layouts/Shared/ImageComponent';

export default function AuthorInfo({ user, thread }) {
    const [followed, setFollowed] = useState(user.is_followed);
    const dispatch = useDispatch();

    const handleFollow = () => {
        if (notLoggedIn()) {
            dispatch(
                sharedTogglePopupRequireLoginAction({
                    show: true,
                    redirectTo: _url.article(thread),
                })
            );
        } else {
            toast
                .promise(
                    memberService.follow({ path: user }),
                    toastPromiseOptions({
                        messageSuccess: followed ? trans('Unfollowed') : trans('Followed'),
                        messageError: followed ? trans('Cannot unfollow') : trans('Cannot follow'),
                    })
                )
                .then(res => {
                    if (res?.status === 200) {
                        setFollowed(!followed);
                    }
                });
        }
    };

    return (
        <>
            <article className="art-user-horizontal art-user-horizontal-3 mt-3">
                <a className="avatar" href={_url.user(user)}>
                    <AvatarComponent width="60" height="60" user={user} />
                </a>
                <div className="des">
                    <span className="name">{showAuthor(user)}</span>
                    <p>
                        <strong className="space-border">
                            <i>{user.message_count}</i> Bài
                        </strong>
                        <strong className="space-border">
                            <i>{user.reaction_score}</i> Like
                        </strong>
                        <strong className="space-border">
                            <i>{user.total_follower}</i> Theo dõi
                        </strong>
                    </p>
                </div>
                <button type="button" className="btn-style-1" onClick={handleFollow}>
                    {followed ? 'Bỏ theo dõi' : 'Theo dõi'}
                </button>
            </article>
        </>
    );
}
