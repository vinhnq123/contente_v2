import { useCallback, useEffect, useRef, useState } from 'react';
import TextareaAutosize from 'react-textarea-autosize';
import { toast } from 'react-toastify';
import { empty, _obj } from '../../common/Support/helpers';
import { INIT_PAGINATION, _url } from '../../common/utils';
import conversationService from '../../services/conversationService';
import MessageItem from './MessageItem';

export default function List(props) {
    const [messages, setMessages] = useState(props.messages);
    const [pagination, setPagination] = useState(props.pagination);
    const [loadFinish, setLoadFinish] = useState(true);
    const [contentQuote, setContentQuote] = useState('');
    const inputRef = useRef('');
    const [seeMore, setSeeMore] = useState(false);
    const [contentMessage, setContentMessage] = useState('');

    // ComponentDidUpdate
    useEffect(() => {
        loadFinish || fetchMessages(1);
    }, [loadFinish]);

    const fetchMessages = useCallback(
        page => {
            conversationService
                .getList({
                    path: { conversation_id: props.conversation.conversation_id },
                    query: { page, with_messages: 1 },
                })
                .then(res => {
                    if (res?.status === 200) {
                        const data = _obj.get(res, 'data.messages');
                        setMessages(current => (page === 1 ? data : current.concat(data)));
                        setPagination(_obj.get(res, 'data.pagination', INIT_PAGINATION));
                    }
                    setLoadFinish(true);
                });
        },
        [pagination.current_page]
    );

    useEffect(() => {
        !empty(contentQuote) && inputRef.current.focus();
    }, [contentQuote]);

    const handlePostReply = () => {
        conversationService
            .replyConversation({
                query: { message: contentQuote + contentMessage, conversation_id: props.conversation.conversation_id },
            })
            .then(res => {
                if (res?.status === 200) {
                    const newList = [...messages];
                    newList.push(res.data.message);
                    setMessages(newList);
                    setContentMessage('');
                    setContentQuote('');
                } else {
                    toast.error(res.statusText);
                }
            });
    };

    const handleChangeContentMessage = e => {
        setContentMessage(e.target.value);
    };

    const handleReplyClick = message => {
        const convMessage = _obj.get(message, 'message_id', message?.first_message_id);
        const content = message.message.replace(/^\[QUOTE=\"[\s\S]+\"\][\s\S]+\[\/QUOTE\][\n ]*/, '');
        let contentQuote = `[QUOTE="${message.username}, convMessage: ${convMessage}, member: ${message.user_id}"]\n${content}\n[/QUOTE]\n`;
        setContentQuote(contentQuote);
    };

    return (
        <>
            <div className="chat-box">
                <div className="chat-detail box-border">
                    <div className={`inner-scroll-2 ${seeMore ? 'active' : ''}`}>
                        <MessageItem
                            message={{
                                ...props.conversation,
                                message_date: props.conversation.last_message_date,
                                message_parsed: props.conversation.title,
                                User: { ...props.conversation.Starter },
                            }}
                        />
                        {messages.map(message => (
                            <MessageItem
                                message={message}
                                ononSetMessageReply={handleReplyClick}
                                key={message.message_id}
                            />
                        ))}
                        <span
                            className="btn-seemores"
                            onClick={() => {
                                setSeeMore(true);
                                if (pagination.current_page < pagination.last_page) {
                                    fetchMessages(pagination.current_page + 1);
                                }
                            }}
                        >
                            Xem thêm <i className="icon icon-more"></i>
                        </span>
                    </div>
                    <div className="box-chat" id="reply">
                        <blockquote
                            style={{
                                color: '#a1a1a1',
                                fontStyle: 'italic',
                                borderLeft: '4px solid',
                                paddingLeft: '1rem',
                                marginBlockStart: '1rem',
                                marginBlockEnd: 0,
                            }}
                        >
                            {contentQuote
                                .replace(/^\[QUOTE(.*)?\]/, '')
                                .replace(/\[\/QUOTE\]\n$/, '')
                                .trim()}
                        </blockquote>
                        <TextareaAutosize
                            className="txt-des"
                            ref={inputRef}
                            value={contentMessage}
                            onChange={handleChangeContentMessage}
                            placeholder="Nhập tin nhắn"
                        />
                        <div className="tool-chat">
                            <button
                                type="button"
                                className="btn-style-1"
                                disabled={empty(contentQuote)}
                                onClick={() => {
                                    setContentQuote('');
                                }}
                            >
                                Huỷ
                            </button>
                            <button
                                type="button"
                                className="btn-style"
                                onClick={handlePostReply}
                                disabled={empty(contentMessage)}
                            >
                                Gửi
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}
