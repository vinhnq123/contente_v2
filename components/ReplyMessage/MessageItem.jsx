import { useEffect, useState } from 'react';
import { decode } from 'html-entities';
import { userInfo } from '../../common/auth';
import { isset, _obj } from '../../common/Support/helpers';
import { timeAgo, _url } from '../../common/utils';
import conversationService from '../../services/conversationService';
import { AvatarComponent } from '../Layouts/Shared/ImageComponent';

export default function MessageItem({ message, ononSetMessageReply }) {
    const [mounted, setMounted] = useState(false);
    const [liked, setLiked] = useState(message.is_reacted_to);
    const [likeCount, setLikeCount] = useState(message.reaction_score);

    useEffect(() => {
        setMounted(true);
    }, []);

    const handleLike = () => {
        conversationService
            .likeMessage({ path: { message_id: message.message_id }, query: { reaction_id: 1 } })
            .then(res => {
                if (res?.status === 200) {
                    setLiked(!liked);
                    setLikeCount(current => (liked ? --current : ++current));
                }
            });
    };

    return (
        <>
            <div className={`item-chat ${mounted && userInfo('username') === message.username ? 'right' : 'left'}`}>
                <article className="chat-user">
                    <a className="avatar" href={_url.user(message)} title={message.username}>
                        <AvatarComponent width="45" height="45" user={_obj.get(message, 'User')} />
                    </a>
                    <div className="des">
                        <a className="name" href={_url.user(message)} title={message.username}>
                            {message.username}
                        </a>
                        <span className="time">{timeAgo(message.message_date)}</span>
                    </div>
                </article>
                <div
                    className="details"
                    dangerouslySetInnerHTML={{
                        __html: decode(_obj.get(message, 'message_parsed', '')),
                    }}
                />
                {isset(message?.first_message_id) || (
                    <div className="tool">
                        {(mounted && userInfo('username') === message.username) || (
                            <span className={liked ? 'fw-bold' : ''} onClick={handleLike}>
                                {likeCount > 0 && likeCount} {liked ? 'Bỏ thích' : 'Thích'}
                            </span>
                        )}
                        <span onClick={() => ononSetMessageReply(message)}>Trả lời</span>
                    </div>
                )}
            </div>
        </>
    );
}
