import React, { useRef, useState } from "react";
import { Swiper, SwiperSlide } from "swiper/react";
import "swiper/css";
import ImageComponent from '/components/Layouts/Shared/ImageComponent';
import { THUMBNAIL_DEFAULT } from '../../common/utils';

export default function HighlightSlide() {
    return (
        <>
            <Swiper slidesPerView={'3'} spaceBetween={30} className="mySwiper slide-top">
                <SwiperSlide>
                    <article className="art-slide">
                        <a className="thumb thumb-5x2">
                            <ImageComponent
                                src="/static/images/thumbnails/3.jpg"
                                alt="thumb"
                                width="380"
                                height="150"
                                loadingBlur
                                srcDefault={THUMBNAIL_DEFAULT}
                            />
                        </a>
                        <header>
                            <a className="avatar-user">
                                <ImageComponent
                                    src="/static/images/avatar-3.png"
                                    alt="thumb"
                                    width="120"
                                    height="120"
                                    loadingBlur
                                    srcDefault={THUMBNAIL_DEFAULT}
                                />
                            </a>
                            <h3>
                                <a href="#" title="">Maybe Fashion</a></h3>
                            <span className="n-menber">132,1k thành viên</span>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing</p>

                        </header>
                    </article>
                </SwiperSlide>
                <SwiperSlide>
                    <article className="art-slide">
                        <a className="thumb thumb-5x2">
                            <ImageComponent
                                src="/static/images/thumbnails/2.jpg"
                                alt="thumb"
                                width="380"
                                height="150"
                                loadingBlur
                                srcDefault={THUMBNAIL_DEFAULT}
                            />
                        </a>
                        <header>
                            <a className="avatar-user">
                                <ImageComponent
                                    src="/static/images/avatar-2.png"
                                    alt="thumb"
                                    width="120"
                                    height="120"
                                    loadingBlur
                                    srcDefault={THUMBNAIL_DEFAULT}
                                />
                            </a>
                            <h3>
                                <a href="#" title="">Maybe Fashion</a></h3>
                            <span className="n-menber">132,1k thành viên</span>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing</p>

                        </header>
                    </article>
                </SwiperSlide>
                <SwiperSlide>
                    <article className="art-slide">
                        <a className="thumb thumb-5x2">
                            <ImageComponent
                                src="/static/images/thumbnails/1.jpg"
                                alt="thumb"
                                width="380"
                                height="150"
                                loadingBlur
                                srcDefault={THUMBNAIL_DEFAULT}
                            />
                        </a>
                        <header>
                            <a className="avatar-user">
                                <ImageComponent
                                    src="/static/images/avatar-1.png"
                                    alt="thumb"
                                    width="120"
                                    height="120"
                                    loadingBlur
                                    srcDefault={THUMBNAIL_DEFAULT}
                                />
                            </a>
                            <h3>
                                <a href="#" title="">Maybe Fashion</a></h3>
                            <span className="n-menber">132,1k thành viên</span>
                            <p>Lorem ipsum dolor sit amet, consectetuer adipiscing</p>
                        </header>
                    </article>
                </SwiperSlide>
            </Swiper>
        </>
    )
}