import { useEffect } from 'react';
import { useForm } from 'react-hook-form';
import { useDispatch, useSelector } from 'react-redux';
import { toast } from 'react-toastify';
import { Modal, ModalHeader, ModalBody } from 'reactstrap';
import { _obj } from '../../common/Support/helpers';
import { getMessage, toastPromiseOptions } from '../../common/utils';
import { profileToggleModalChangeUsername } from '../../redux/actions/profileActions';
import userService from '../../services/userService';

const validation = {
    username: {
        required: true,
        maxLength: 200,
    },
    change_reason: {
        required: true,
    },
};

export default function ModalUserChange() {
    const isOpen = useSelector(state => state.profileReducers.modalChangeUsername.show);
    const dispatch = useDispatch();
    const toggle = () => dispatch(profileToggleModalChangeUsername({ show: !isOpen }));
    const {
        register,
        handleSubmit,
        formState: { errors },
        reset,
    } = useForm();

    useEffect(() => () => reset({ username: '', change_reason: '' }), [isOpen]);

    const handleChangeUsername = data => {
        toast
            .promise(
                userService.updateUsername({ data }),
                toastPromiseOptions({
                    messageSuccess: 'Cập nhật tài khoản thành công',
                    error: {
                        render({ data }) {
                            const message = _obj.get(data, 'data.errors.0.message', 'Whoop! Something went wrong.');
                            if (
                                /Your last username change was too recent\. You may try again in [0-9]+ day\(s\)\./.test(
                                    message
                                )
                            ) {
                                const days = _obj.get(data, 'data.errors.0.params.days', 30);
                                return `Bạn đã thay đổi tài khoản gần đây. Hãy thử lại sau ${days} ngày`;
                            }
                            return 'Cập nhật tài khoản không thành công';
                        },
                    },
                })
            )
            .then(res => {
                if (res?.status === 200) {
                    toggle();
                }
            });
    };

    return (
        <>
            <Modal className="modal-notify modal-dialog-centered modal-sm" isOpen={isOpen} toggle={toggle}>
                <ModalHeader toggle={toggle}>
                    <div className="title-modal">Thay đổi User</div>
                </ModalHeader>
                <ModalBody>
                    <form className="frm-general text-left px-3" onSubmit={handleSubmit(handleChangeUsername)}>
                        <div className="form-group">
                            <label>Tài khoản mới</label>
                            <input
                                type="text"
                                className="form-control"
                                {...register('username', validation.username)}
                            />
                            {errors.username && (
                                <small className="text-danger">
                                    {getMessage({
                                        type: errors.username?.type,
                                        field: 'tài khoản',
                                        maxLength: 200,
                                    })}
                                </small>
                            )}
                        </div>
                        <div className="form-group">
                            <label>Lý do thay đổi</label>
                            <textarea
                                className="form-control"
                                {...register('change_reason', validation.change_reason)}
                            />
                            {errors.change_reason && (
                                <small className="text-danger">
                                    {getMessage({
                                        type: errors.change_reason?.type,
                                        field: 'lý do thay đổi',
                                    })}
                                </small>
                            )}
                        </div>
                        <div className="d-flex justify-content-center mt-5 mb-2">
                            <button type="submit" className="btn-style">
                                Cập nhật
                            </button>
                        </div>
                    </form>
                </ModalBody>
            </Modal>
        </>
    );
}
