import { useEffect, useState } from 'react';
import { showAuthor, _url } from '../../common/utils';
import { _obj } from '../../common/Support/helpers';
import moment from 'moment';

export default function UserBox({ profile, onSetFile }) {
    const [mounted, setMounted] = useState(false);

    useEffect(() => {
        setMounted(true);
    }, []);

    return (
        <>
            {mounted && profile && (
                <div className="block-bar">
                    <h2>{showAuthor(profile)}</h2>
                    <div className="inner-bar text-center">
                        <span className="date-join">
                            Tham gia vào ngày {moment.unix(profile.register_date).format('DD/MM/YYYY')}
                        </span>
                        <p>{profile.about}</p>
                        <ul className="list-social mt-2 justify-content-center">
                            <li>
                                <a href="#" title="">
                                    <i className="icon icon-facebook"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" title="">
                                    <i className="icon icon-yoputube"></i>
                                </a>
                            </li>
                            <li>
                                <a href="#" title="">
                                    <i className="icon icon-ins"></i>
                                </a>
                            </li>
                        </ul>
                        <div className="group-info group-info-vertical">
                            <div className="inner">
                                {profile.message_count}
                                <strong>Posts</strong>
                            </div>
                            <div className="inner">
                                {profile.total_follower}
                                <strong>Người theo dõi</strong>
                            </div>
                            <div className="inner">
                                110<strong>Đang theo dõi</strong>
                            </div>
                        </div>
                        <div className="text-center mt-5">
                            <a className="btn-style-1" href="#" title="">
                                <span>Viết bài</span>
                            </a>
                        </div>
                    </div>
                </div>
            )}
        </>
    );
}
