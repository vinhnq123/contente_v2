import { useCallback, useEffect, useState } from 'react';
import { toast } from 'react-toastify';
import { _obj } from '../../common/Support/helpers';
import { THUMBNAIL_DEFAULT, toastPromiseOptions, NOTIFY_TIME_OUT } from '../../common/utils';
import userService from '../../services/userService';
import ImageComponent, { getCroppedImg, RATIO_BANNER } from '../Layouts/Shared/ImageComponent';
import Cropper from 'react-easy-crop';

const _crop = { x: 0, y: 0 };
const _zoom = 1;

export default function Banner({ profile }) {
    const [imagePreviewUrl, setImagePreviewUrl] = useState(
        _obj.get(profile, 'profile_banner_urls.l', THUMBNAIL_DEFAULT)
    );
    const _croppedAreaPixels = {
        x: _obj.get(profile, 'banner_position_x', 0),
        y: _obj.get(profile, 'banner_position_y', 0),
        width: 0,
        height: 0,
    };
    const [file, setFile] = useState();
    const [btnGroup, setBtnGroup] = useState(false);
    const [inputValue, setInputValue] = useState('');
    const [crop, setCrop] = useState(_crop);
    const [zoom, setZoom] = useState(_zoom);
    const [croppedAreaPixels, setCroppedAreaPixels] = useState(null);
    const [croppedImage, setCroppedImage] = useState(null);

    useEffect(() => {
        showCroppedImage(_croppedAreaPixels);
    }, []);

    const handleCancelUpdate = () => {
        setBtnGroup(false);
        setFile();
        setImagePreviewUrl(_obj.get(profile, 'profile_banner_urls.l', THUMBNAIL_DEFAULT));
        setInputValue('');
        setCrop(_crop);
        setZoom(_zoom);
    };

    const handleUploadImage = e => {
        e.preventDefault();
        const reader = new FileReader();
        const fileUpload = e.target.files[0];
        reader.onloadend = () => {
            setFile(fileUpload);
            setImagePreviewUrl(reader.result);
        };
        reader.readAsDataURL(fileUpload);
        setInputValue(e.target.value);
        setBtnGroup(true);
    };

    const onCropComplete = useCallback((croppedArea, croppedAreaPixels) => {
        setCroppedAreaPixels(croppedAreaPixels);
    }, []);

    const showCroppedImage = async (croppedData = _croppedAreaPixels) => {
        const croppedImage = await getCroppedImg(imagePreviewUrl, croppedAreaPixels ? croppedAreaPixels : croppedData);
        setCroppedImage(croppedImage);
    };

    const handleUpdateBanner = () => {
        toast
            .promise(
                userService.updateBanner({ data: { upload: file, croppedAreaPixels } }),
                toastPromiseOptions({
                    messageSuccess: 'Cập nhật thành công',
                    messageError: 'Cập nhật thất bại',
                })
            )
            .then(res => {
                if (res?.status === 200) {
                    showCroppedImage();
                    setBtnGroup(false);
                    setFile();
                    setInputValue('');
                    setTimeout(() => window.location.reload(), NOTIFY_TIME_OUT);
                }
            });
    };

    return (
        <>
            <div className="block-user">
                <div className="img-cover">
                    {btnGroup ? (
                        <Cropper
                            image={imagePreviewUrl}
                            crop={crop}
                            zoom={zoom}
                            aspect={RATIO_BANNER}
                            onCropChange={setCrop}
                            onCropComplete={onCropComplete}
                            onZoomChange={setZoom}
                        />
                    ) : (
                        <ImageComponent
                            src={croppedImage || imagePreviewUrl}
                            alt="thumb"
                            width="1300"
                            height="400"
                            loadingBlur
                            srcDefault={THUMBNAIL_DEFAULT}
                            className="img-component-cover"
                        />
                    )}
                </div>
                <div className="container">
                    <span className={btnGroup ? 'd-none' : 'btn-change-cover'}>
                        <input
                            className="up-img"
                            type="file"
                            id="img"
                            name="img"
                            accept="image/*"
                            onChange={handleUploadImage}
                            value={inputValue}
                        />
                        <i className="icon icon-img"></i> Chỉnh sửa ảnh bìa
                    </span>
                    <div className={btnGroup ? 'outer-save' : 'd-none'}>
                        <button type="button" className="btn btn-style" onClick={handleUpdateBanner}>
                            Lưu
                        </button>
                        <button type="button" className="btn btn-style-1" onClick={handleCancelUpdate}>
                            <span>Huỷ</span>
                        </button>
                    </div>
                </div>
            </div>
        </>
    );
}
