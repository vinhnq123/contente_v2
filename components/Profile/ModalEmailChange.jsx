import { useEffect } from 'react';
import { useForm } from 'react-hook-form';
import { useDispatch } from 'react-redux';
import { useSelector } from 'react-redux';
import { Modal, ModalHeader, ModalBody } from 'reactstrap';
import { getMessage, NOTIFY_TIME_OUT, REGEX_EMAIL, toastPromiseOptions } from '../../common/utils';
import { profileToggleModalChangeEmail } from '../../redux/actions/profileActions';
import authService from '../../services/authService';
import { toast } from 'react-toastify';
import userService from '../../services/userService';

const handleAuthentication = async data => {
    return await authService.login({ data });
};

export default function ModalEmailChange({ profile }) {
    const isOpen = useSelector(state => state.profileReducers.modalChangeEmail.show);
    const dispatch = useDispatch();
    const toggle = () => dispatch(profileToggleModalChangeEmail({ show: !isOpen }));
    const {
        register,
        reset,
        formState: { errors },
        handleSubmit,
    } = useForm();

    useEffect(() => () => reset({ email: '', password: '' }), [isOpen]);

    const handleChangeEmail = data => {
        handleAuthentication({ username: profile.email, password: data.password }).then(res => {
            if (res?.status === 200) {
                toast
                    .promise(
                        userService.updateEmail({ data: { email: data.email } }),
                        toastPromiseOptions({
                            messageSuccess: 'Cập nhật email thành công.',
                            messageError: 'Cập nhật email thất bại.',
                        })
                    )
                    .then(res => {
                        if (res?.status === 200) {
                            setTimeout(() => window.location.reload(), NOTIFY_TIME_OUT);
                        }
                    });
            } else {
                toast.error('Mật khẩu không chính xác');
                toggle();
            }
        });
    };

    return (
        <>
            <Modal
                className="modal-notify modal-dialog-centered modal-sm"
                isOpen={isOpen}
                toggle={toggle}
                unmountOnClose={true}
            >
                <ModalHeader toggle={toggle}>
                    <div className="title-modal">Thay đổi Email</div>
                </ModalHeader>
                <ModalBody>
                    <form className="frm-general text-left px-3" onSubmit={handleSubmit(handleChangeEmail)}>
                        <div className="form-group">
                            <label>Email mới</label>
                            <input
                                type="text"
                                className="form-control"
                                {...register('email', {
                                    required: true,
                                    maxLength: 200,
                                    pattern: REGEX_EMAIL,
                                })}
                            />
                            {errors.email && (
                                <small className="text-danger">
                                    {getMessage({
                                        type: errors.email?.type,
                                        field: 'email',
                                        maxLength: 200,
                                        regex: 'Vui lòng nhập đúng email',
                                    })}
                                </small>
                            )}
                        </div>
                        <div className="form-group">
                            <label>Mật khẩu hiện tại</label>
                            <input
                                type="password"
                                className="form-control"
                                id="password"
                                {...register('password', {
                                    required: true,
                                    maxLength: 200,
                                })}
                            />
                            {errors.password && (
                                <small className="text-danger">
                                    {getMessage({
                                        type: errors.password?.type,
                                        field: 'mật khẩu',
                                        maxLength: 200,
                                    })}
                                </small>
                            )}
                        </div>
                        <div className="d-flex justify-content-center mt-5 mb-2">
                            <button type="submit" className="btn-style">
                                Cập nhật
                            </button>
                        </div>
                    </form>
                </ModalBody>
            </Modal>
        </>
    );
}
