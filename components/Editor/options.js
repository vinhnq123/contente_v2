import uploadService from '../../services/uploadService';

// Editor options
export const options = {
    placeholder: 'Nhập nội dung bài viết',
    autofocus: true,
    i18n: {
        messages: {
            ui: {
                blockTunes: {
                    toggler: {
                        'Click to tune': 'Bấm để điều chỉnh',
                        'or drag to move': 'hoặc nắm kéo để di chuyển',
                    },
                },
                inlineToolbar: {
                    converter: {
                        'Convert to': 'Chuyển định dạng',
                    },
                },
                toolbar: {
                    toolbox: {
                        Add: 'Thêm',
                    },
                },
            },
            toolNames: {
                'Text': 'Chữ thường',
                'Heading': 'Tiêu đề',
                'List': 'Danh sách',
                'Warning': 'Cảnh báo',
                'Checklist': 'Dấu check',
                'Quote': 'Trích dẫn',
                'Code': 'Code',
                'Delimiter': 'Dấu ngăn cách',
                'Raw HTML': 'HTML thuần',
                'Table': 'Bảng',
                'Link': 'Liên kết',
                'Marker': 'Đánh dấu',
                'Bold': 'In đậm',
                'Italic': 'In nghiêng',
                'InlineCode': 'InlineCode',
            },
            tools: {
                warning: {
                    Title: 'Tiêu đề',
                    Message: 'Nội dung',
                },
                link: {
                    'Add a link': 'Thêm liên kết',
                },
            },
            blockTunes: {
                delete: {
                    Delete: 'Xoá',
                },
                moveUp: {
                    'Move up': 'Di chuyển lên',
                },
                moveDown: {
                    'Move down': 'Di chuyển xuống',
                },
            },
        },
    },

    /**
     * onReady callback
     */
    onReady: () => {},

    /**
     * onChange callback
     */
    onChange(ev) {
        if (ev.blocks.getBlocksCount() < this.previousCount) {
            let noMatch = [...this.tools.imageUploaded];
            document.querySelectorAll('.image-tool__image-picture').forEach(img => {
                let itemNoMatch = noMatch.find(item => item.url === img.src);
                noMatch.splice(noMatch.indexOf(itemNoMatch, 1));
            });
            if (noMatch.length == 1) {
                deleteAttachment(noMatch[0]?.attachment_id);
                this.tools.imageUploaded.splice(this.tools.imageUploaded.indexOf(noMatch[0]), 1);
            }
        } else {
            this.previousCount = ev.blocks.getBlocksCount();
        }
    },
};

const deleteAttachment = attachment_id => {
    uploadService.deleteAttachment({ path: { attachment_id } }).then(res => {
        if (res?.status === 200) {
            console.log('The file have been deleted.');
        }
    });
};
