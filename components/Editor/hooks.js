import { useCallback, useState, useEffect } from 'react';
export const dataKey = 'editorData';

export const useSaveCallback = (editor, saveLocal = false) => {
    return useCallback(async () => {
        if (!editor) return;
        try {
            const out = await editor.save();
            console.group('EDITOR onSave');
            if (saveLocal) {
                localStorage.setItem(dataKey, JSON.stringify(out));
                console.info('Saved in localStorage');
            }
            console.groupEnd();
            return out;
        } catch (e) {
            console.error('SAVE RESULT failed', e);
        }
    }, [editor]);
};

// Set editor data after initializing
export const useSetData = (editor, data) => {
    useEffect(() => {
        if (!editor || !data) {
            return;
        } else if (data.blocks.length === 0) {
            return;
        }

        editor.isReady.then(() => {
            // fixing an annoying warning in Chrome `addRange(): The given range isn't in document.`
            setTimeout(() => {
                editor.render(data);
            }, 100);
        });
    }, [editor, data]);
};

export const useClearDataCallback = editor => {
    return useCallback(
        e => {
            e.preventDefault();
            if (!editor) {
                return;
            }
            editor.isReady.then(() => {
                // fixing an annoying warning in Chrome `addRange(): The given range isn't in document.`
                setTimeout(() => {
                    editor.clear();
                }, 100);
            });
        },
        [editor]
    );
};

// load saved data
export const useLoadData = (initData = null) => {
    const [data, setData] = useState(initData);
    const [loading, setLoading] = useState(false);

    // Mimic async data load
    useEffect(() => {
        setLoading(true);
        const id = setTimeout(() => {
            const saved = localStorage.getItem(dataKey);
            if (saved) {
                const parsed = JSON.parse(saved);
                setData(parsed);
            }
            setLoading(false);
        }, 200);

        return () => {
            setLoading(false);
            clearTimeout(id);
        };
    }, []);

    return { data, loading };
};
