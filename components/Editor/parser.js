import edjsHTML from 'editorjs-html';
import { _obj } from '../../common/Support/helpers';

const EditorParser = () => {
    const toHTML = output => {
        return edjsHTML({
            embed: function ({ data }) {
                const caption = data.caption ? '<div class="caption">' + data.caption + '</div>' : '';
                switch (data.service) {
                    case 'vimeo':
                        return `<div class="mb-video"><iframe src="${data.embed}" height="${data.height}" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
                            ${caption}</div>`;
                    case 'youtube':
                        return `<div class="mb-video"><iframe width="${data.width}" height="${data.height}" src="${data.embed}" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            ${caption}</div>`;
                    default:
                        return `<div class="mb-video"><iframe src="${data.source}" width="${data.width}" height="${data.height}" frameborder="0" allow="autoplay; fullscreen; picture-in-picture" allowfullscreen></iframe>
                            ${caption}</div>`;
                }
            },
            image: function ({ data }) {
                const caption = _obj.get(data, 'caption', '');
                const figcaption = caption ? `<figcaption>${caption}</figcaption>` : '';
                return (
                    '<figure class="mb-image"><img src="' +
                    _obj.get(data, 'file.url', _obj.get(data, 'file')) +
                    '" alt="' +
                    caption +
                    '" />' +
                    figcaption +
                    '</figure>'
                );
            },
            quote: function ({ data }) {
                const caption = data.caption ? '<div class="caption">' + data.caption + '</div>' : '';
                return '<div class="mb-quote"><blockquote>' + data.text + '</blockquote>' + caption + '</div>';
            },
            code: function ({ data }) {
                return '<pre><code>' + data.code + '</code></pre>';
            },
        })
            .parse(output)
            .join('');
    };
    return { toHTML };
};

export default EditorParser;
