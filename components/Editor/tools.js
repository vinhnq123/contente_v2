import Header from '@editorjs/header';
import List from '@editorjs/list';
import Paragraph from '@editorjs/paragraph';
import Embed from '@editorjs/embed';
import ImageTool from '@editorjs/image';
import Quote from '@editorjs/quote';
import uploadService from '../../services/uploadService';

export const tools = {
    header: {
        class: Header,
        inlineToolbar: ['link'],
    },
    list: {
        class: List,
        inlineToolbar: true,
    },
    paragraph: {
        class: Paragraph,
        inlineToolbar: true,
    },
    embed: {
        class: Embed,
        config: {
            services: {
                youtube: true,
                gfycat: true,
                instagram: true,
                facebook: true,
                pinterest: true,
                coub: true,
            },
        },
    },
    imageUploaded: [],
    image: {
        class: ImageTool,
        config: {
            endpoints: {
                byUrl: '/api/fetch/image',
            },
            uploader: {
                uploadByFile: function (file) {
                    return uploadService
                        .attachment({
                            data: {
                                key: localStorage.getItem('keyupload'),
                                attachment: file,
                            },
                        })
                        .then(res => {
                            if (res?.status === 200) {
                                tools.imageUploaded.push({
                                    url: res.data.attachment.direct_url,
                                    attachment_id: res.data.attachment.attachment_id,
                                });
                            }
                            return {
                                success: res?.status === 200 ? 1 : 0,
                                file: {
                                    url: res.data.attachment.direct_url,
                                },
                                stretched: true,
                            };
                        });
                },
            },
        },
    },
    quote: {
        class: Quote,
        inlineToolbar: true,
        config: {
            quotePlaceholder: 'Nhập trích dẫn',
            captionPlaceholder: 'Nhập tác giả',
        },
    },
};
