import moment from 'moment';
import { useState } from 'react';
import { FacebookShareButton } from 'react-share';
import { _obj } from '../../common/Support/helpers';
import { showAuthor, THUMBNAIL_DEFAULT, timeAgo, toastPromiseOptions, _url } from '../../common/utils';
import postService from '../../services/postService';
import ImageComponent, { AvatarComponent } from '../Layouts/Shared/ImageComponent';
import { toast } from 'react-toastify';

export default function PostItem({ post }) {
    const [bookmarked, setBookmarked] = useState(true);

    const handleUnbookmark = () => {
        return toast
            .promise(postService.bookmark({ path: { post_id: post.post_id } }, bookmarked), toastPromiseOptions())
            .then(res => {
                if (res?.status === 200) {
                    setBookmarked(!bookmarked);
                }
            });
    };

    return (
        <>
            <article className="art-feed">
                <header>
                    <a className="author" href={_url.user(post.User)} title={showAuthor(post.User)}>
                        <span className="avatar">
                            <AvatarComponent width="90" height="90" loadingBlur user={post?.User} />
                        </span>
                        <div className="des">
                            <span className="name">
                                {showAuthor(post.User)} <i>{timeAgo(post.post_date)}</i>
                            </span>
                            <p>
                                <strong>{moment.unix(post.post_date).format('D MMMM')}</strong>
                                &nbsp;&#8226;&nbsp;
                                <strong>{post.Forum.title}</strong>
                            </p>
                        </div>
                    </a>
                    <a className="btn-style-1" href={_url.article(post)} title={post.title}>
                        <span>Xem thêm</span>
                    </a>
                </header>
                <section>
                    <a href={_url.article(post)}>
                        <div className="title-head">{post.title}</div>
                    </a>
                    <a href={_url.article(post)}>
                        <figure className="outer-img">
                            <ImageComponent
                                src={_obj.get(post, 'Attachments.0.thumbnail_url', THUMBNAIL_DEFAULT)}
                                alt="thumb"
                                width="815"
                                height="500"
                                loadingBlur
                                srcDefault={THUMBNAIL_DEFAULT}
                            />
                        </figure>
                    </a>
                </section>
                <footer>
                    <ul className="list-tool">
                        <li>
                            <a href={_url.article(post).append('#comment')}>
                                <i className="icon icon-binhluan" />
                                <span>Bình luận</span>
                            </a>
                        </li>
                        <li>
                            <FacebookShareButton url={_url.article(post).get()} hashtag="#maybe">
                                <i className="icon icon-chiase" />
                                <span>Chia sẻ</span>
                            </FacebookShareButton>
                        </li>
                        <li onClick={handleUnbookmark}>
                            <i className={bookmarked ? 'icon icon-save active' : 'icon icon-save'} />
                            <span> Lưu tin</span>
                        </li>
                    </ul>
                </footer>
            </article>
        </>
    );
}
