import { useEffect, useState } from 'react';
import { _obj, _str } from '../../common/Support/helpers';
import { INIT_PAGINATION, THUMBNAIL_DEFAULT, timeAgo, _url } from '../../common/utils';
import threadService from '../../services/threadService';
import ImageComponent from '../Layouts/Shared/ImageComponent';

export default function ListWaitingForApprove() {
    const [threads, setThreads] = useState([]);
    const [pagination, setPagination] = useState(INIT_PAGINATION);
    const [loadFinish, setLoadFinish] = useState(false);

    useEffect(() => {
        loadFinish ||
            (page => {
                threadService.getListWaitingForApprove().then(res => {
                    if (res?.status === 200) {
                        const data = _obj.get(res, 'data.thread', []);
                        setThreads(current => (page === 1 ? data : current.concat(data)));
                        setPagination(_obj.get(res, 'data.pagination', INIT_PAGINATION));
                    }
                    setLoadFinish(true);
                });
            })(pagination.current_page + 1);
    }, [loadFinish]);

    const handleToggleLock = async (thread_id, isLook = true) => {
        let xhr;
        if (isLook) {
            xhr = await threadService.lockThreadWaitingForApprove({ query: { thread_id } });
        } else {
            xhr = await threadService.unlockThreadWaitingForApprove({ query: { thread_id } });
        }
        if (xhr?.status === 200) {
            alert(isLook ? 'Bài viết đã bị khoá.' : 'Bài viết đã được mở khoá.');
            setLoadFinish(false);
        } else {
            alert(isLook ? 'Bạn không thể khoá bài viết này.' : 'Bạn không thể mở khoá bài viết này.');
        }
    };

    return (
        <>
            {threads.map(thread => (
                <article className="art-feed-s" key={thread.thread_id}>
                    <div className="inner">
                        <a
                            className="thumb thumb-1x1"
                            href={_url.page(`chinh-sua-bai-viet/${thread.thread_id}`)}
                            title={thread.title}
                        >
                            <ImageComponent
                                src={_obj.get(thread, 'FirstPost.Attachments.0.thumbnail_url', THUMBNAIL_DEFAULT)}
                                alt="thumb"
                                width="175"
                                height="175"
                                loadingBlur
                                srcDefault={THUMBNAIL_DEFAULT}
                            />
                        </a>
                    </div>
                    <header>
                        <div className="avatar">
                            <div className="des">
                                <a className="name" href={_url.category(thread.Forum)} title={thread.Forum.title}>
                                    {thread.Forum.title}
                                </a>
                                <span className="time">{timeAgo(thread.FirstPost.post_date)}</span>
                            </div>
                        </div>
                        <div className="name">
                            <a href={_url.page(`chinh-sua-bai-viet/${thread.thread_id}`)} title={thread.title}>
                                {thread.title}
                            </a>
                        </div>
                        <div className="list-tag">
                            {_obj.get(thread, 'tags', []).map((tag, key) => (
                                <a href={_url.tags({ tag_url: _str.kebab(tag) })} title={tag} key={key}>
                                    {`#${tag}`}
                                </a>
                            ))}
                        </div>
                    </header>
                </article>
            ))}
        </>
    );
}
