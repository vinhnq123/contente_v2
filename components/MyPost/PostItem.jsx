import { _obj, _str } from '../../common/Support/helpers';
import { THUMBNAIL_DEFAULT, timeAgo, _url } from '../../common/utils';
import ImageComponent from '../Layouts/Shared/ImageComponent';

export default function PostItem({ thread }) {
    return (
        <>
            <article className="art-feed-s">
                <div className="inner">
                    <a className="thumb thumb-1x1" href="#" title={thread.title}>
                        <ImageComponent
                            src={_obj.get(thread, 'FirstPost.Attachments.0.thumbnail_url', THUMBNAIL_DEFAULT)}
                            alt="thumb"
                            width="175"
                            height="175"
                            loadingBlur
                            srcDefault={THUMBNAIL_DEFAULT}
                        />
                    </a>
                </div>
                <header>
                    <div className="avatar">
                        <div className="des">
                            <a className="name" href={_url.category(thread.Forum)} title={thread.Forum.title}>
                                {thread.Forum.title}
                            </a>
                            <span className="time">{timeAgo(thread.FirstPost.post_date)}</span>
                        </div>
                    </div>
                    <div className="name">
                        <a href="#" title={thread.title}>
                            {thread.title}
                        </a>
                    </div>
                    <div className="list-tag">
                        {_obj.get(thread, 'tags', []).map((tag, key) => (
                            <a href={_url.tags({ tag_url: _str.kebab(tag) })} title={tag} key={key}>
                                {`#${tag}`}
                            </a>
                        ))}
                    </div>
                </header>
            </article>
        </>
    );
}
