import { useEffect, useState } from 'react';
import { _obj, _str } from '../../common/Support/helpers';
import { THUMBNAIL_DEFAULT, timeAgo, _url } from '../../common/utils';
import threadService from '../../services/threadService';
import ImageComponent from '../Layouts/Shared/ImageComponent';

export default function ListDraft() {
    const [drafts, setDrafts] = useState([]);
    const [loadFinish, setLoadFinish] = useState(false);

    useEffect(() => {
        loadFinish ||
            (() => {
                threadService.getListDraft().then(res => {
                    if (res?.status === 200) {
                        setDrafts(_obj.get(res, 'data.draft', []));
                    }
                    setLoadFinish(true);
                });
            })();
    }, [loadFinish]);

    return (
        <>
            {drafts.map(draft => (
                <article className="art-feed-s" key={draft.draft_id}>
                    <div className="inner">
                        <a
                            className="thumb thumb-1x1"
                            href={_url.draftDetail(draft.node_id)}
                            title={draft.extra_data.title}
                        >
                            <ImageComponent
                                src={THUMBNAIL_DEFAULT}
                                alt="thumb"
                                width="175"
                                height="175"
                                loadingBlur
                                srcDefault={THUMBNAIL_DEFAULT}
                            />
                        </a>
                    </div>
                    <header>
                        <div className="avatar">
                            <div className="des">
                                <a className="name" href={_url.draftDetail(draft.node_id)} title={draft.node_name}>
                                    {draft.node_name}
                                </a>
                                <span className="time">{timeAgo(draft.last_update)}</span>
                            </div>
                        </div>
                        <div className="name">
                            <a href={_url.draftDetail(draft.node_id)} title={draft.extra_data.title}>
                                {draft.extra_data.title}
                            </a>
                        </div>
                        <div className="list-tag">
                            {draft.extra_data.tags.map((tag, key) => (
                                <a href={_url.tags({ tag_url: _str.kebab(tag) })} title={tag} key={key}>
                                    {`#${tag}`}
                                </a>
                            ))}
                        </div>
                    </header>
                </article>
            ))}
        </>
    );
}
