import { useEffect, useState } from 'react';
import { _obj } from '../../common/Support/helpers';
import { INIT_PAGINATION, _url } from '../../common/utils';
import threadService from '../../services/threadService';
import ResultItem from '../Layouts/Shared/ResultItem';

export default function ListResults(props) {
    const [threads, setThreads] = useState(props.threads);
    const [pagination, setPagination] = useState(props.pagination);
    const [loadFinish, setLoadFinish] = useState(true);

    useEffect(() => {
        loadFinish ||
            (page => {
                threadService
                    .getSearchResults({
                        query: { q: props.keyword, page },
                    })
                    .then(res => {
                        if (res?.status === 200) {
                            const data = _obj.get(res, 'data.threads', []);
                            setThreads(current => (page === 1 ? data : current.concat(data)));
                            setPagination(_obj.get(res, 'data.pagination', INIT_PAGINATION));
                        }
                        setLoadFinish(true);
                    });
            })(pagination.current_page + 1);
    }, [loadFinish]);

    return (
        <>
            <h1 className="title-style mt-0">{`TÌM KIẾM ĐƯỢC ${pagination.total} KẾT QUẢ "${props.keyword}"`}</h1>
            {threads.length ? (
                threads.map(thread => <ResultItem thread={thread} key={thread.thread_id} />)
            ) : (
                <p className="w-100 text-center">
                    Không tìm thấy kết quả với từ khoá <b>{`"${props.keyword}"`}</b>
                </p>
            )}
            <div className="d-flex justify-content-center">
                <button
                    type="button"
                    className="btn-style btn-big mt-2 mb-3"
                    disabled={pagination.current_page >= pagination.last_page || threads.length === 0}
                    onClick={() => setLoadFinish(false)}
                >
                    Xem thêm
                </button>
            </div>
        </>
    );
}
