import { useState } from 'react';
import { useDispatch } from 'react-redux';
import { toast } from 'react-toastify';
import { notLoggedIn } from '../../common/auth';
import { _obj } from '../../common/Support/helpers';
import { THUMBNAIL_DEFAULT, toastPromiseOptions, trans, _url } from '../../common/utils';
import { sharedTogglePopupRequireLoginAction } from '../../redux/actions/sharedActions';
import nodeService from '../../services/nodeService';
import ImageComponent from '../Layouts/Shared/ImageComponent';

export default function CategoryItem({ node, listNodesWatched }) {
    const [followed, setFollowed] = useState(listNodesWatched.find(item => item.node_id === node.node_id) ? 1 : 0);
    const dispatch = useDispatch();

    const handleFollowCategory = () => {
        if (notLoggedIn()) {
            dispatch(
                sharedTogglePopupRequireLoginAction({
                    show: true,
                    redirectTo: _url.page('category'),
                })
            );
        } else {
            toast
                .promise(
                    nodeService.follow({
                        path: { node_id: node.node_id },
                        query: { stop: followed, send_alert: 1, send_email: 0 },
                    }),
                    toastPromiseOptions({
                        messageSuccess: followed ? trans('Unfollowed topic') : trans('Followed topic'),
                        messageError: trans('You cannot follow this topic'),
                    })
                )
                .then(res => {
                    if (res?.status === 200) {
                        setFollowed(!followed);
                    }
                });
        }
    };

    return (
        <>
            <article className="art-user-group" key={node.node_id}>
                <a href={_url.category(node)} title={node.title}>
                    <span className="thumb thumb-3x1">
                        <ImageComponent
                            src={_obj.get(
                                node,
                                'type_data.last_thread_title_attachments.0.direct_url',
                                THUMBNAIL_DEFAULT
                            )}
                            alt="thumb"
                            width="800"
                            height="265"
                            loadingBlur
                            srcDefault={THUMBNAIL_DEFAULT}
                        />
                    </span>
                </a>
                <header>
                    <h2>
                        <a href={_url.category(node)} title={node.title}>
                            {node.title}
                        </a>
                    </h2>
                    <div className="btn-follow" onClick={handleFollowCategory}>
                        <span>{followed ? 'Bỏ theo dõi' : 'Theo dõi'}</span>
                    </div>
                </header>
            </article>
        </>
    );
}
