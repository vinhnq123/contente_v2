import { useEffect } from 'react';
import { useState } from 'react';
import { _obj } from '../../common/Support/helpers';
import { INIT_PAGINATION } from '../../common/utils';
import threadService from '../../services/threadService';
import ArticleItem from '../Layouts/Shared/ArticleItem';

export default function Threads(props) {
    const [threads, setThreads] = useState([]);
    const [pagination, setPagination] = useState(INIT_PAGINATION);
    const [loadFinish, setLoadFinish] = useState(false);

    useEffect(() => {
        loadFinish ||
            (page => {
                threadService.getListByUser({ path: { user_id: props.user.user_id }, query: { page } }).then(res => {
                    if (res?.status === 200) {
                        const data = _obj.get(res, 'data.threads', []);
                        setThreads(current => (page === 1 ? data : current.concat(data)));
                        setPagination(_obj.get(res, 'data.pagination', INIT_PAGINATION));
                    }
                    setLoadFinish(true);
                });
            })(pagination.current_page + 1);
    }, [loadFinish]);

    return (
        <>
            {threads.length > 0 ? (
                threads.map(thread => <ArticleItem thread={thread} key={thread.thread_id} />)
            ) : (
                <span className="box-no-cm">Chưa có bài viết</span>
            )}
            <div className="d-flex justify-content-center">
                <button
                    className="btn-style btn-big mt-2 mb-5"
                    disabled={pagination.current_page >= pagination.last_page}
                    onClick={() => {
                        if (pagination.current_page < pagination.last_page) {
                            setLoadFinish(false);
                        }
                    }}
                >
                    Xem thêm
                </button>
            </div>
        </>
    );
}
