import { useState } from 'react';
import { decode } from 'html-entities';
import TextareaAutosize from 'react-textarea-autosize';
import { _obj } from '../../common/Support/helpers';
import { showAuthor, timeAgo, _url } from '../../common/utils';
import profileService from '../../services/profileService';
import { AvatarComponent } from '../Layouts/Shared/ImageComponent';

export default function ProfilePostCommentItem({ comment, onUpdateComments }) {
    const [liked, setLiked] = useState(comment.is_reacted_to);
    const [likeCount, setLikeCount] = useState(comment.reaction_score);
    const [contentMessage, setContentMessage] = useState('');
    const [toggleReply, setToggleReply] = useState(false);

    const handleLike = () => {
        profileService
            .likeComment({
                path: { profile_post_comment_id: comment.profile_post_comment_id },
                query: { reaction_id: 1 },
            })
            .then(res => {
                if (res?.status === 200) {
                    setLiked(!liked);
                    setLikeCount(current => (liked ? --current : ++current));
                }
            });
    };

    const handleChangeContentMessage = e => {
        setContentMessage(e.target.value);
    };

    const handlePostComment = () => {
        profileService
            .postComment({
                query: { profile_post_id: comment.profile_post_id, message: contentMessage },
            })
            .then(res => {
                if (res?.status === 200) {
                    setContentMessage('');
                    setToggleReply(false);
                    onUpdateComments();
                }
            });
    };

    return (
        <>
            <div className="box-comment">
                <div className="inner-comment">
                    <a className="thumb-avatar" href="#" title="">
                        <AvatarComponent width="50" height="50" loadingBlur user={_obj.get(comment, 'User')} />
                    </a>
                    <div className="right-des">
                        <div className="comment-name">
                            <a href={_url.user(comment.User)} title={showAuthor(comment.User)}>
                                {showAuthor(comment.User)}
                            </a>
                            <span className="time">{timeAgo(comment.comment_date)}</span>
                        </div>
                        <div dangerouslySetInnerHTML={{ __html: decode(_obj.get(comment, 'message_parsed', '')) }} />
                        <ul className="list-tool list-tool-left no-icon">
                            <li onClick={handleLike}>
                                <i className={`icon icon-like ${liked ? 'active' : ''}`} />
                                <span className="space">{likeCount > 0 && likeCount} Thích</span>
                            </li>
                            <li onClick={() => setToggleReply(!toggleReply)}>
                                <i className="icon icon-binhluan" />
                                <span>{toggleReply ? 'Huỷ' : 'Trả lời'}</span>
                            </li>
                        </ul>
                        <div className={toggleReply ? 'box-txt' : 'd-none'}>
                            <TextareaAutosize value={contentMessage} onChange={handleChangeContentMessage} />
                            <button type="button" className="btn-style-1 btn-comment" onClick={handlePostComment}>
                                <span>
                                    <i>Gửi lời nhắn</i>
                                </span>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
}
