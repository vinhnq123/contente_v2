export default function Trophies({ trophies }) {
    return (
        <>
            <h2 className="title-1">TROPHIES</h2>
            <div className="box-trophies">
                {trophies.map((trophy, index) => (
                    <div className="cols" key={trophy.point + index}>
                        <div className="inner-trophies">
                            <a className="link-trophies" href="#">
                                <span>{trophy.point}</span>
                                <strong>{trophy.title}</strong>
                            </a>
                        </div>
                    </div>
                ))}
            </div>
        </>
    );
}
