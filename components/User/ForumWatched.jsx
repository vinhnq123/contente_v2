import React from 'react';
import { Swiper, SwiperSlide } from 'swiper/react';
import SwiperCore, { Autoplay } from 'swiper';
import 'swiper/css';
import ImageComponent from '../Layouts/Shared/ImageComponent';
import { THUMBNAIL_DEFAULT, _url } from '../../common/utils';
import { _obj } from '../../common/Support/helpers';

SwiperCore.use([Autoplay]);

export default function ForumWatched({ forums }) {
    return (
        <>
            <Swiper
                slidesPerView={'1.5'}
                spaceBetween={15}
                speed={600}
                breakpoints={{
                    1024: {
                        slidesPerView: 2.5,
                        spaceBetween: 20,
                    },
                }}
                autoplay={{
                    delay: 5000,
                    disableOnInteraction: false,
                }}
                className="mySwiper slide-community"
            >
                {forums.map(item => {
                    const forum = item.Forum;
                    return (
                        <SwiperSlide key={forum.node_id}>
                            <article className="art-slide">
                                <a href={_url.category(forum)} className="thumb thumb-5x2">
                                    <ImageComponent
                                        src={_obj.get(
                                            forum,
                                            'type_data.last_thread_title_attachments.0.thumbnail_url',
                                            THUMBNAIL_DEFAULT
                                        )}
                                        alt="thumb"
                                        width="380"
                                        height="150"
                                        loadingBlur
                                        srcDefault={THUMBNAIL_DEFAULT}
                                    />
                                </a>
                                <header>
                                    <h3>
                                        <a href={_url.category(forum)} title={forum.title}>
                                            {forum.title}
                                        </a>
                                    </h3>
                                    <span className="n-menber">{forum.total_member} thành viên</span>
                                    <p>{forum.type_data.last_thread_title}</p>
                                </header>
                            </article>
                        </SwiperSlide>
                    );
                })}
            </Swiper>
        </>
    );
}
