import { useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';
import moment from 'moment';
import { toast } from 'react-toastify';
import { isLoggedIn, notLoggedIn } from '../../common/auth';
import { empty, _obj, _str } from '../../common/Support/helpers';
import { INIT_PAGINATION, makeSummaryFromContent, showAuthor, timeAgo, _url } from '../../common/utils';
import { sharedTogglePopupRequireLoginAction } from '../../redux/actions/sharedActions';
import { AvatarComponent } from '../Layouts/Shared/ImageComponent';
import ModalReport from '../Layouts/Shared/ModalReport';
import ThumbnailItems from '../Layouts/Shared/ThumbnailItems';
import TextareaAutosize from 'react-textarea-autosize';
import profileService from '../../services/profileService';
import ProfilePostCommentItem from './ProfilePostCommentItem';

const LIMIT_COMMENT = 3;

export default function ProfilePostItem({ post }) {
    const [liked, setLiked] = useState(post.is_reacted_to);
    const [likeCount, setLikeCount] = useState(post.reaction_score);
    const [commentCount, setCommentCount] = useState(post.comment_count);
    const dispatch = useDispatch();
    const [contentMessage, setContentMessage] = useState('');
    const [mounted, setMounted] = useState(false);
    const [comments, setComments] = useState(_obj.get(post, 'Comments', []));
    const [pagination, setPagination] = useState({
        current_page: 1,
        per_page: LIMIT_COMMENT,
        last_page: commentCount > LIMIT_COMMENT ? Math.ceil(commentCount / LIMIT_COMMENT) : 1,
        total: commentCount,
        shown: LIMIT_COMMENT,
    });
    const [loadFinish, setLoadFinish] = useState(true);

    useEffect(() => {
        setMounted(true);
        setComments(current => current.slice(-3).reverse());
    }, []);

    useEffect(() => {
        loadFinish ||
            (page => {
                profileService
                    .getListComment({
                        path: { profile_post_id: post.profile_post_id },
                        query: { page, limit: LIMIT_COMMENT },
                    })
                    .then(res => {
                        if (res?.status === 200) {
                            const data = _obj.get(res, 'data.comments', []);
                            setComments(current => (page === 1 ? data : current.concat(data)));
                            setPagination(_obj.get(res, 'data.pagination', INIT_PAGINATION));
                        }
                        setLoadFinish(true);
                    });
            })(pagination.current_page + 1);
    }, [loadFinish]);

    const handleLike = () => {
        if (notLoggedIn()) {
            dispatch(
                sharedTogglePopupRequireLoginAction({
                    show: true,
                    redirectTo: _url.user(post),
                })
            );
        } else {
            profileService
                .like({ path: { profile_post_id: post.profile_post_id }, query: { reaction_id: 1 } })
                .then(res => {
                    if (res?.status === 200) {
                        setLiked(!liked);
                        setLikeCount(current => (liked ? --current : ++current));
                    }
                });
        }
    };

    const handlePostComment = () => {
        if (contentMessage) {
            profileService
                .postComment({ query: { profile_post_id: post.profile_post_id, message: contentMessage } })
                .then(res => {
                    if (res?.status === 200) {
                        setCommentCount(++commentCount);
                        setContentMessage('');
                        handleUpdateComments();
                    } else {
                        toast.error('Bình luận không thành công');
                    }
                });
        }
    };

    const handleUpdateComments = () => {
        const currentPagination = { ...pagination };
        profileService
            .getListComment({
                path: { profile_post_id: post.profile_post_id },
                query: { page: 1, limit: currentPagination.current_page * LIMIT_COMMENT },
            })
            .then(res => {
                if (res?.status === 200) {
                    setComments(_obj.get(res, 'data.comments', []));
                    setPagination(currentPagination);
                }
            });
    };

    const handleChangeContentMessage = e => {
        setContentMessage(e.target.value);
    };

    return (
        <>
            <article className="art-feed">
                <header>
                    <div className="author">
                        <a className="avatar" href={_url.user(post.User)} title={showAuthor(post.User)}>
                            <AvatarComponent width="90" height="90" loadingBlur user={_obj.get(post, 'User')} />
                        </a>
                        <div className="des">
                            <div className="name">
                                <a href={_url.user(post.User)} title={showAuthor(post.User)}>
                                    {showAuthor(post.User)}
                                </a>
                                <i>{timeAgo(post.post_date)}</i>
                            </div>
                            <p>
                                <strong>{moment.unix(post.post_date).format('D MMMM')}</strong>
                            </p>
                        </div>
                    </div>
                </header>
                <section>
                    <div className="title-head">{post.title}</div>
                    <p className="txt-cm">{makeSummaryFromContent(_obj.get(post, 'message', ''), 250)}</p>
                    <span>
                        <ThumbnailItems thread={post} isPost />
                    </span>
                </section>
                <footer>
                    <ul className="list-tool list-tool-left no-icon">
                        <li onClick={handleLike} className={`outer-like ${liked ? 'active' : ''}`}>
                            <i className={`icon icon-like ${liked ? 'active' : ''}`} />
                            <span>{likeCount > 0 && likeCount} Thích</span>
                        </li>
                        <li>
                            <div>
                                <i className="icon icon-binhluan" />
                                <span>{commentCount > 0 && commentCount} Bình luận</span>
                            </div>
                        </li>
                        <li>
                            <ModalReport />
                        </li>
                    </ul>
                </footer>
                <div className="art-detail">
                    <div className="block-binhluan">
                        {mounted && isLoggedIn() && (
                            <div className="box-txt">
                                <TextareaAutosize
                                    value={contentMessage}
                                    onChange={handleChangeContentMessage}
                                    placeholder="Nhập bình luận"
                                />
                                <button type="button" className="btn-style-1 btn-comment" onClick={handlePostComment}>
                                    <span>
                                        <i>Gửi bình luận</i>
                                    </span>
                                </button>
                            </div>
                        )}
                        {commentCount > 0 && (
                            <div>
                                {!empty(post.Comments) &&
                                    comments.map(comment => (
                                        <ProfilePostCommentItem
                                            comment={comment}
                                            key={comment.profile_post_comment_id}
                                            onUpdateComments={handleUpdateComments}
                                        />
                                    ))}
                                {pagination.current_page < pagination.last_page && (
                                    <span className="btn-xemthem" onClick={() => setLoadFinish(false)}>
                                        Xem thêm bình luận
                                    </span>
                                )}
                            </div>
                        )}
                    </div>
                </div>
            </article>
        </>
    );
}
