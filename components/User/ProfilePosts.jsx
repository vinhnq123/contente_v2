import { useEffect } from 'react';
import { _url } from '../../common/utils';
import { _obj } from '../../common/Support/helpers';
import ProfilePostItem from './ProfilePostItem';
import { useDispatch, useSelector } from 'react-redux';
import { userFetchProfilePostAction } from '../../redux/actions/userActions';

export default function ProfilePosts({ userId }) {
    const posts = useSelector(state => state.userReducers.posts);
    const pagination = useSelector(state => state.userReducers.pagination);
    const loadFinish = useSelector(state => state.userReducers.loadFinish);
    const dispatch = useDispatch();

    useEffect(() => {
        loadFinish || dispatch(userFetchProfilePostAction({ query: { page: 1, user_id: userId } }));
    }, [loadFinish]);

    return (
        <>
            {posts.length > 0 ? (
                posts.map(post => <ProfilePostItem post={post} key={post.profile_post_id} />)
            ) : (
                <span className="box-no-cm">Chưa có bài viết</span>
            )}
            <div className="d-flex justify-content-center">
                <button
                    className="btn-style btn-big mt-2 mb-5"
                    disabled={pagination.current_page >= pagination.last_page}
                    onClick={() => {
                        if (pagination.current_page < pagination.last_page) {
                            dispatch(
                                userFetchProfilePostAction({
                                    query: { page: pagination.current_page + 1, user_id: userId },
                                })
                            );
                        }
                    }}
                >
                    Xem thêm
                </button>
            </div>
        </>
    );
}
