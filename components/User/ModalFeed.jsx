import { useState } from 'react';
import { Modal, ModalHeader, ModalBody } from 'reactstrap';
import { AvatarComponent } from '../Layouts/Shared/ImageComponent';
import UploadImg from '../Layouts/Shared/UploadImg';
import TextareaAutosize from 'react-textarea-autosize';
import { getMessage, showAuthor, _url } from '../../common/utils';
import { useForm } from 'react-hook-form';
import { _obj } from '../../common/Support/helpers';
import profileService from '../../services/profileService';
import { useEffect } from 'react';
import uploadService from '../../services/uploadService';
import { userInfo } from '../../common/auth';

export default function ModalFeed({ user }) {
    const [modal, setModal] = useState(false);
    const toggle = () => setModal(!modal);
    const {
        register,
        handleSubmit,
        formState: { errors },
    } = useForm();
    const [attachmentKey, setAttachmentKey] = useState('');

    useEffect(() => {
        (() => {
            uploadService
                .getKeyUpload({
                    data: { context: 'profile_user_id', type: 'profile_post', context_value: user.user_id },
                })
                .then(res => {
                    if (res?.status === 200) {
                        setAttachmentKey(_obj.get(res, 'data.key', ''));
                    }
                });
        })();
    }, []);

    const handlePostStatus = data => {
        profileService
            .postStatus({ query: { ...data, user_id: user.user_id, attachment_key: attachmentKey } })
            .then(res => {
                if (res?.status === 200) {
                    window.location.reload();
                }
            });
    };

    return (
        <>
            <span className="btn-Upimg" onClick={toggle}>
                Gửi cảm nghĩ
            </span>
            <Modal className="modal-dialog-centered modal-feed modal-lg" isOpen={modal} toggle={toggle}>
                <ModalHeader toggle={toggle}>
                    <div className="title-modal">
                        <strong>Tạo bài viết</strong>
                    </div>
                </ModalHeader>
                <ModalBody>
                    <form onSubmit={handleSubmit(handlePostStatus)}>
                        <div className="box-post">
                            <div className="avatar">
                                <AvatarComponent width="48" height="48" loadingBlur user={userInfo()} />
                            </div>
                            <span className="name">
                                {userInfo('username')} <i>{userInfo('user_title')}</i>
                            </span>
                        </div>
                        <div className="mb-3">
                            <TextareaAutosize
                                className="txtarea-feed"
                                placeholder={
                                    user.username === userInfo('username')
                                        ? 'Bạn đang nghĩ gì?'
                                        : `Viết gì đó cho ${showAuthor(user)}`
                                }
                                {...register('message', {
                                    required: true,
                                })}
                            />
                            {errors.message && (
                                <small className="text-danger">
                                    {getMessage({
                                        type: errors.message?.type,
                                        field: 'nội dung',
                                    })}
                                </small>
                            )}
                        </div>
                        <div className="upload-img">
                            <UploadImg attachmentKey={attachmentKey} />
                        </div>
                        <div className="d-flex justify-content-center">
                            <button type="submit" className="btn-style w-100">
                                Đăng
                            </button>
                        </div>
                    </form>
                </ModalBody>
            </Modal>
        </>
    );
}
