import HeadMeta from '../../components/Layouts/Shared/HeadMeta';
import Footer from '../../components/Layouts/Footer';
import ImageComponent, { AvatarComponent, getCroppedImg } from '../../components/Layouts/Shared/ImageComponent';
import { getUserIdFromHash, showAuthor, THUMBNAIL_DEFAULT, toastPromiseOptions, trans, _url } from '../../common/utils';
import { _obj, _str } from '../../common/Support/helpers';
import userService from '../../services/userService';
import moment from 'moment';
import ProfilePosts from '../../components/User/ProfilePosts';
import ForumWatched from '../../components/User/ForumWatched';
import Trophies from '../../components/User/Trophies';
import ModalFeed from '../../components/User/ModalFeed';
import { isLoggedIn, notLoggedIn, userInfo } from '../../common/auth';
import { useEffect, useState } from 'react';
import { Nav, NavItem, NavLink, TabContent, TabPane } from 'reactstrap';
import Threads from '../../components/User/Threads';
import { useDispatch } from 'react-redux';
import { sharedTogglePopupRequireLoginAction } from '../../redux/actions/sharedActions';
import memberService from '../../services/memberService';
import { toast } from 'react-toastify';
import nodeService from '../../services/nodeService';
import BarCate from '../../components/Layouts/Shared/BarCate';

export default function User(props) {
    const [curTab, setCurTab] = useState('feed');
    const [mounted, setMounted] = useState(false);
    const [followed, setFollowed] = useState(props.user.is_followed);
    const dispatch = useDispatch();
    const _croppedAreaPixels = {
        x: _obj.get(props.user, 'banner_position_x', 0),
        y: _obj.get(props.user, 'banner_position_y', 0),
        width: 0,
        height: 0,
    };
    const [croppedImage, setCroppedImage] = useState(null);

    useEffect(() => {
        setMounted(true);
        showCroppedImage();
    }, []);

    const showCroppedImage = async (croppedData = _croppedAreaPixels) => {
        const croppedImage = await getCroppedImg(
            _obj.get(props.user, 'profile_banner_urls.l', THUMBNAIL_DEFAULT),
            croppedData
        );
        setCroppedImage(croppedImage);
    };

    const handleFollow = () => {
        if (notLoggedIn()) {
            dispatch(
                sharedTogglePopupRequireLoginAction({
                    show: true,
                    redirectTo: _url.user(props.user),
                })
            );
        } else {
            if (userInfo('user_id') === props.user.user_id) {
                toast.error('Bạn không thể tự theo dõi mình');
                return false;
            }
            toast
                .promise(
                    memberService.follow({ path: props.user }),
                    toastPromiseOptions({
                        messageSuccess: followed ? trans('Unfollowed') : trans('Followed'),
                        messageError: followed ? trans('Cannot unfollow') : trans('Cannot follow'),
                    })
                )
                .then(res => {
                    if (res?.status === 200) {
                        setFollowed(!followed);
                    }
                });
        }
    };

    return (
        <>
            <HeadMeta
                title={showAuthor(props.user)}
                image={_obj.get(props.user, 'profile_banner_urls.l', THUMBNAIL_DEFAULT)}
                description={props.user.about}
                keywords={`${props.user.username}`}
            />
            <section className="section mt-0">
                <div className="block-user">
                    <div className="img-cover">
                        <ImageComponent
                            src={croppedImage || _obj.get(props.user, 'profile_banner_urls.l', THUMBNAIL_DEFAULT)}
                            width="1300"
                            height="300"
                            loadingBlur
                            srcDefault={THUMBNAIL_DEFAULT}
                            className="img-component-cover"
                        />
                        <div className="container container-bottom">
                            {mounted && isLoggedIn() && userInfo('username') !== props.user.username && (
                                <>
                                    <a
                                        className="btn-conversation"
                                        href={_url.conversation(props.user.user_id)}
                                        title="Bắt trò chuyện"
                                    >
                                        <i className="icon icon-messege-gray"></i>
                                        <span className="text">Bắt trò chuyện</span>
                                    </a>
                                    <div className="btn-style btn-follow" onClick={handleFollow}>
                                        {followed ? 'Bỏ theo dõi' : 'Theo dõi'}
                                    </div>
                                </>
                            )}
                        </div>
                    </div>
                </div>
            </section>
            <section className="section section-main">
                <div className="container">
                    <div className="row">
                        <div className="col col-lg-3 sidebar order-lg-3">
                            <div className="sticky-top">
                                <div className="box-user box-border">
                                    <a className="avatar-user">
                                        <AvatarComponent width="120" height="120" loadingBlur user={props.user} />
                                    </a>
                                    <h1>{props.user.username}</h1>
                                    <span className="date-join">
                                        Tham gia ngày {moment.unix(props.user.register_date).format('DD/MM/YYYY')}
                                    </span>
                                    <p>{props.user.user_title}</p>
                                    <ul className="list-social">
                                        <li>
                                            <a
                                                href={`https://www.facebook.com/${_obj.get(
                                                    props.user,
                                                    'custom_fields.facebook',
                                                    ''
                                                )}`}
                                                title="Facebook"
                                                target="_blank"
                                                rel="noreferrer"
                                            >
                                                <i className="icon icon-facebook" />
                                            </a>
                                        </li>
                                        <li>
                                            <a
                                                href={`https://www.youtube.com/${_obj.get(
                                                    props.user,
                                                    'custom_fields.youtube',
                                                    ''
                                                )}`}
                                                title="Youtube"
                                                target="_blank"
                                                rel="noreferrer"
                                            >
                                                <i className="icon icon-yoputube" />
                                            </a>
                                        </li>
                                        <li>
                                            <a
                                                href={`https://www.instagram.com/${_obj.get(
                                                    props.user,
                                                    'custom_fields.instagram',
                                                    ''
                                                )}`}
                                                title="Instagram"
                                                target="_blank"
                                                rel="noreferrer"
                                            >
                                                <i className="icon icon-ins" />
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div className="col col-lg-6 main order-lg-2">
                            <div className="tab-user">
                                {mounted && isLoggedIn() && (
                                    <div className="box-feed mb-5">
                                        <AvatarComponent width="64" height="64" loadingBlur user={userInfo()} />
                                        <ModalFeed user={props.user} />
                                    </div>
                                )}
                                <h2 className="title-1">CÁC CỘNG ĐỒNG YÊU THÍCH</h2>
                                <ForumWatched forums={props.forumWatched} />
                                <div className="box-find box-Filter">
                                    <Nav tabs>
                                        <NavItem>
                                            <NavLink
                                                className={curTab === 'feed' ? 'active' : null}
                                                onClick={() => setCurTab('feed')}
                                            >
                                                Feed
                                            </NavLink>
                                        </NavItem>
                                        <NavItem>
                                            <NavLink
                                                className={curTab === 'post' ? 'active' : null}
                                                onClick={() => setCurTab('post')}
                                            >
                                                Bài viết
                                            </NavLink>
                                        </NavItem>
                                        <NavItem>
                                            <NavLink
                                                className={curTab === 'info' ? 'active' : null}
                                                onClick={() => setCurTab('info')}
                                            >
                                                Thông tin
                                            </NavLink>
                                        </NavItem>
                                    </Nav>
                                </div>

                                <TabContent activeTab={curTab} className="tab-content-comment">
                                    <TabPane tabId="feed">
                                        <h2 className="title-1">CÁC BÀI VIẾT</h2>
                                        <ProfilePosts userId={props.userId} />
                                    </TabPane>
                                    <TabPane tabId="post">
                                        <h2 className="title-1">CÁC BÀI POST</h2>
                                        <Threads user={props.user} />
                                    </TabPane>
                                    <TabPane tabId="info">
                                        <div className="box-intro box-border">
                                            <h3>GIỚI THIỆU</h3>
                                            <p>{props.user.about || 'Đang cập nhật...'}</p>
                                        </div>
                                        <div className="box-tro">
                                            <div className="inner-tro">
                                                <span>
                                                    {props.user.message_count} <strong>Bài viết</strong>
                                                </span>
                                            </div>
                                            <div className="inner-tro">
                                                <span>
                                                    {props.followers} <strong>Người theo dõi</strong>
                                                </span>
                                            </div>
                                            <div className="inner-tro">
                                                <span>
                                                    {props.followings} <strong>Đang theo dõi</strong>
                                                </span>
                                            </div>
                                        </div>
                                        <div className="box-tro box-tro-1">
                                            <div className="inner-tro">
                                                <span>
                                                    {props.user.trophy_points} <strong>Điểm danh hiệu</strong>
                                                </span>
                                            </div>
                                            <div className="inner-tro">
                                                <span>
                                                    {props.user.vote_score} <strong>Vote</strong>
                                                </span>
                                            </div>
                                            <div className="inner-tro">
                                                <span>
                                                    {props.user.question_solution_count} <strong>Câu hỏi</strong>
                                                </span>
                                                <span>
                                                    {props.user.reaction_score} <strong>Lượt like</strong>
                                                </span>
                                            </div>
                                        </div>
                                        {props.user.Trophy.length ? <Trophies trophies={props.user.Trophy} /> : null}
                                    </TabPane>
                                </TabContent>
                            </div>
                        </div>
                        <div className="col-lg-3 left-bar col order-lg-1">
                            <div className="sticky-top">
                                <div className="bar-left bar-cate no-border">
                                    <BarCate />
                                </div>
                                <Footer />
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </>
    );
}

export async function getServerSideProps({ params, req }) {
    const { user } = params;
    const userId = getUserIdFromHash(user);
    const xhr = await userService.getInfo({ path: { user_id: userId }, req });
    const followings = await userService.getListFollowingOfAuthor({ path: { user_id: userId } });
    const followers = await userService.getListFollowerOfAuthor({ path: { user_id: userId } });
    let forumWatched = [];
    await nodeService.getListWatched({ req }).then(res => {
        if (res?.status === 200) {
            forumWatched = _obj.get(res, 'data.forums', []);
        }
    });
    if (xhr?.status === 200) {
        return {
            props: {
                user: _obj.get(xhr, 'data.user'),
                userId,
                alias: user,
                followings: _obj.get(followings, 'data.total', 0),
                followers: _obj.get(followers, 'data.total', 0),
                forumWatched,
            },
        };
    }
    return {
        notFound: true,
    };
}
