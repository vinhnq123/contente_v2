import { useEffect, useState } from 'react';
import { _obj } from '../../common/Support/helpers';
import { INIT_PAGINATION, _url } from '../../common/utils';
import HeadMeta from '../../components/Layouts/Shared/HeadMeta';
import threadService from '../../services/threadService';
import Footer from '../../components/Layouts/Footer';
import ResultItem from '../../components/Layouts/Shared/ResultItem';
import BarCate from '../../components/Layouts/Shared/BarCate';

export default function Tags(props) {
    const [threads, setThreads] = useState(props.threads);
    const [pagination, setPagination] = useState(props.pagination);
    const [loadFinish, setLoadFinish] = useState(true);

    useEffect(() => {
        loadFinish ||
            (page => {
                threadService.getListByTag({ query: { q: props.tag, page } }).then(res => {
                    if (res?.status === 200) {
                        const data = _obj.get(res, 'data.threads', []);
                        setThreads(current => (page === 1 ? data : current.concat(data)));
                        setPagination(_obj.get(res, 'data.pagination', INIT_PAGINATION));
                    }
                    setLoadFinish(true);
                });
            })(pagination.current_page + 1);
    }, [loadFinish]);

    return (
        <>
            <HeadMeta
                title={_obj.get(props, 'tagInfo.tag', props.tag)}
                path={_url.tags(_obj.get(props, 'tagInfo.tag', { tag_url: props.tag }))}
            />
            <section className="section section-main">
                <div className="container">
                    <div className="row">
                        <div className="col col-lg-8 main col padding-rigt">
                            <h1 className="title-style mt-0">
                                TÌM KIẾM ĐƯỢC {pagination.total} KẾT QUẢ{' '}
                                {`"${_obj.get(props, 'tagInfo.tag', props.tag)}"`}
                            </h1>
                            {threads.map(thread => (
                                <ResultItem thread={thread} key={thread.thread_id} />
                            ))}
                            <div className="d-flex justify-content-center">
                                <button
                                    type="button"
                                    className="btn-style btn-big mt-2 mb-3"
                                    disabled={pagination.current_page >= pagination.last_page || threads.length === 0}
                                    onClick={() => setLoadFinish(false)}
                                >
                                    Xem thêm
                                </button>
                            </div>
                        </div>

                        <div className="col-lg-4 left-bar col">
                            <div className="sticky-top">
                                <div className="bar-left bar-cate no-border">
                                    <BarCate />
                                </div>
                                <Footer />
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </>
    );
}

export async function getServerSideProps({ params, req }) {
    const { tag } = params;
    if (tag) {
        let threads = [];
        let pagination = INIT_PAGINATION;
        return await threadService.getListByTag({ query: { q: tag }, req }).then(res => {
            if (res?.status === 200) {
                threads = _obj.get(res, 'data.threads', []);
                pagination = _obj.get(res, 'data.pagination', INIT_PAGINATION);
                return {
                    props: {
                        tag,
                        tagInfo: _obj.get(res, 'data.tag'),
                        threads,
                        pagination,
                    },
                };
            }
            return {
                notFound: true,
            };
        });
    }
    return {
        notFound: true,
    };
}
