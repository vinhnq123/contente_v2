import { useState } from 'react';
import { useDispatch } from 'react-redux';
import { Col } from 'reactstrap';
import HeadMeta from '../../components/Layouts/Shared/HeadMeta';
import Footer from '../../components/Layouts/Footer';
import ActiveMember from '../../components/Layouts/Shared/ActiveMember';
import Group from '../../components/Layouts/Shared/Group';
import { AvatarComponent } from '../../components/Layouts/Shared/ImageComponent';
import {
    INIT_PAGINATION,
    makeSummaryFromContent,
    showAuthor,
    THUMBNAIL_DEFAULT,
    timeAgo,
    toastPromiseOptions,
    trans,
    _url,
} from '../../common/utils';
import threadService from '../../services/threadService';
import { empty, _obj, _str } from '../../common/Support/helpers';
import { decode } from 'html-entities';
import Comments from '../../components/Article/Comments';
import { notLoggedIn } from '../../common/auth';
import postService from '../../services/postService';
import ActionBar from '../../components/Article/ActionBar';
import moment from 'moment';
import { sharedTogglePopupRequireLoginAction } from '../../redux/actions/sharedActions';
import AuthorInfo from '../../components/Article/AuthorInfo';
import RelatedArticles from '../../components/Article/RelatedArticles';
import ToolBar from '../../components/Article/ToolBar';
import { toast } from 'react-toastify';

const getHtmlContent = thread => decode(_obj.get(thread, 'FirstPost.message_parsed', '')).replace(/\<br ?\/?\>/g, '');

export default function Article(props) {
    const [visitorContentVote, setVisitorContentVote] = useState(props.thread.FirstPost.visitor_content_vote);
    const [voteCount, setVoteCount] = useState(props.thread.FirstPost.User.vote_score);
    const dispatch = useDispatch();

    const handleVote = type => {
        if (notLoggedIn()) {
            dispatch(
                sharedTogglePopupRequireLoginAction({
                    show: true,
                    redirectTo: _url.article(props.thread),
                })
            );
        } else {
            toast
                .promise(
                    postService.vote({ path: { post_id: props.thread.FirstPost.post_id }, query: { type } }),
                    toastPromiseOptions({
                        messageSuccess: empty(visitorContentVote) ? trans('Vote article') : trans('Unvote article'),
                    })
                )
                .then(res => {
                    if (res?.status === 200) {
                        if (empty(visitorContentVote)) {
                            setVisitorContentVote(type);
                            setVoteCount(current => (type === 'up' ? ++current : --current));
                        } else {
                            setVisitorContentVote(null);
                            setVoteCount(props.thread.FirstPost.User.vote_score);
                        }
                    }
                });
        }
    };

    return (
        <>
            <HeadMeta
                title={props.thread.title}
                description={makeSummaryFromContent(_obj.get(props.thread, 'FirstPost.message', ''), 180)}
                path={_url.article(props.thread)}
                image={_obj.get(props.thread, 'FirstPost.Attachments.0.direct_url', THUMBNAIL_DEFAULT)}
                keywords={props.thread.title}
                canonical="/p/"
            />
            <section className="section section-detail">
                <div className="container">
                    <div className="row">
                        <Col className="col-lg-1">
                            <div className="tool-vote sticky-top">
                                <i
                                    className={
                                        visitorContentVote === 'up' ? 'icon icon-voteUp active' : 'icon icon-voteUp'
                                    }
                                    onClick={() => handleVote('up')}
                                />
                                <span className="space">{voteCount}</span>
                                <i
                                    className={
                                        visitorContentVote === 'down'
                                            ? 'icon icon-voteDown active'
                                            : 'icon icon-voteDown'
                                    }
                                    onClick={() => handleVote('down')}
                                />
                            </div>
                        </Col>
                        <Col className="col-lg-7 main padding-right">
                            <article className="art-detail">
                                <div className="box-border">
                                    <header>
                                        <div className="author">
                                            <a className="avatar" href={_url.user(props.thread.User)} title="">
                                                <AvatarComponent
                                                    width="48"
                                                    height="48"
                                                    loadingBlur
                                                    user={_obj.get(props.thread, 'User')}
                                                />
                                            </a>
                                            <div className="des">
                                                <div className="name">
                                                    <a href={_url.user(props.thread.User)} title="">
                                                        {showAuthor(props.thread.User)}{' '}
                                                    </a>
                                                    <i>{timeAgo(props.thread.last_post_date)}</i>
                                                </div>
                                                <p>
                                                    <strong>
                                                        {moment.unix(props.thread.FirstPost.post_date).format('D MMMM')}
                                                    </strong>
                                                    &nbsp;&#8226;&nbsp;
                                                    <a
                                                        href={_url.category(props.thread.Forum)}
                                                        title={props.thread.Forum.title}
                                                    >
                                                        {props.thread.Forum.title}
                                                    </a>
                                                </p>
                                            </div>
                                        </div>

                                        <h1>{props.thread.title}</h1>
                                    </header>
                                    <ActionBar thread={props.thread} />
                                    <div className="hag-tag">
                                        {props.thread.tags.map(tag => (
                                            <a href={_url.tags({ tag_url: _str.kebab(tag) })} title={tag} key={tag}>
                                                {`#${tag}`}
                                            </a>
                                        ))}
                                    </div>
                                    <section>
                                        <div
                                            dangerouslySetInnerHTML={{
                                                __html: getHtmlContent(props.thread),
                                            }}
                                        />
                                    </section>
                                </div>
                                <RelatedArticles thread={props.thread} />

                                <ToolBar thread={props.thread} />
                                <AuthorInfo user={props.thread.User} thread={props.thread} />
                                <Comments {...props} />
                            </article>
                        </Col>
                        <Col className="col-lg-4 sidebar">
                            <Group node={props.thread.Forum} />
                            <ActiveMember />
                            <div className="sticky-top">
                                <Footer />
                            </div>
                        </Col>
                    </div>
                </div>
            </section>
        </>
    );
}

export async function getServerSideProps({ req, params, query }) {
    const { thread_id } = params;
    const { comment } = query;
    let comments = [];
    if (comment) {
        await postService.getDetail({ path: { post_id: comment }, req }).then(res => {
            if (res?.status === 200) {
                comments.push(_obj.get(res, 'data.post'));
            }
        });
    }
    return await threadService.getDetail({ path: { thread_id }, query: { with_posts: true }, req }).then(res => {
        if (res?.status === 200) {
            const thread = _obj.get(res, 'data.thread');
            if (comments.length > 0) {
                comments = comments.concat(
                    _obj.get(res, 'data.posts', []).filter(comment => comment.post_id !== comments[0].post_id)
                );
            } else {
                comments = _obj.get(res, 'data.posts', []);
            }
            return {
                props: {
                    thread,
                    threadId: thread.thread_id,
                    comments,
                    pagination: _obj.get(res, 'data.pagination', INIT_PAGINATION),
                },
            };
        }
        return {
            notFound: true,
        };
    });
}
