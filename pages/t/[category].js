import HeadMeta from '../../components/Layouts/Shared/HeadMeta';
import Footer from '../../components/Layouts/Footer';
import BarCate from '../../components/Layouts/Shared/BarCate';
import List from '../../components/Category/List';
import ActiveMember from '../../components/Layouts/Shared/ActiveMember';
import Group from '../../components/Layouts/Shared/Group';
import Admins from '../../components/Category/Admins';
import Mods from '../../components/Category/Mods';
import { INIT_PAGINATION, makeSummaryFromContent } from '../../common/utils';
import nodeService from '../../services/nodeService';
import threadService from '../../services/threadService';
import { _obj } from '../../common/Support/helpers';
import SectionTop from '../../components/Category/SectionTop';

export default function Category(props) {
    return (
        <>
            <HeadMeta
                title={props.node.title}
                description={makeSummaryFromContent(_obj.get(props.node, 'description', ''), 180)}
                keywords={props.node.title}
            />
            <SectionTop node={props.node} listNodesWatched={props.listNodesWatched} />
            <section className="section section-main">
                <div className="container">
                    <div className="row">
                        <div className="col col-lg-6 main order-lg-2">
                            <List {...props} />
                        </div>
                        <div className="col col-lg-3 sidebar order-lg-3">
                            <Group node={props.node} totalPost={props.pagination.total} />
                            <div className="block-bar">
                                <h2>LUẬT LỆ CỦA NHÓM</h2>
                                <div className="inner-bar">
                                    <ul className="list-rules">
                                        {props.node.type_data.custom_fields.map(field => (
                                            <li key={field.field_id}>{field.description}</li>
                                        ))}
                                    </ul>
                                    <div className="text-center mt-5">
                                        <a className="btn-style" href="#" title="">
                                            Xem thêm
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <Admins node={props.node} />
                            <Mods node={props.node} />
                            <div className="sticky-top">
                                <ActiveMember />
                            </div>
                        </div>
                        <div className="col-lg-3 left-bar col order-lg-1">
                            <div className="sticky-top">
                                <div className="bar-left bar-cate no-border">
                                    <BarCate />
                                </div>
                                <Footer />
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </>
    );
}

export async function getServerSideProps({ params, req }) {
    const { category } = params;
    const xhrNode = await nodeService.getDetail({ path: { node_slug: category }, req });
    if (xhrNode?.status === 200) {
        const node = xhrNode.data.node;
        let threads = [];
        let pagination = INIT_PAGINATION;
        let listNodesWatched = [];
        const xhrThread = await threadService.getListByNode({
            path: { node_id: node.node_id },
            query: { type: 'newest' },
            req,
        });
        const xhrNodeWatched = await nodeService.getListWatched({ req });
        if (xhrThread?.status === 200) {
            threads = xhrThread.data.threads;
            pagination = xhrThread.data.pagination;
        }
        if (xhrNodeWatched?.status === 200) {
            listNodesWatched = xhrNodeWatched.data.forums.map(forum =>
                _obj.only(forum.Forum, ['title', 'node_id', 'node_name', 'total_member', 'type_data'])
            );
        }
        return {
            props: {
                node,
                threads,
                pagination,
                listNodesWatched,
            },
        };
    }
    return {
        notFound: true,
    };
}
