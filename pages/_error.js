import Head from 'next/head';
import Link from 'next/link';

const errorMessages = {
    403: 'Bạn không có quyền truy cập.',
    404: 'Yêu cầu của bạn không được tìm thấy.',
    500: 'Lỗi hệ thống.',
};

export default function Error({ statusCode }) {
    return (
        <>
            <Head>
                <title>{`${statusCode}! ${errorMessages[statusCode]}`}</title>
            </Head>
            <div>
                <h1>
                    <span>{`${statusCode}!`}</span>
                    {errorMessages[statusCode]}
                </h1>
                <Link href="/">
                    <a>Quay về trang chủ</a>
                </Link>
            </div>
        </>
    );
}
