import Master from '../components/Layouts';
import 'bootstrap/dist/css/bootstrap.min.css';
import '../styles/scss/style.scss';

function MyApp({ Component, pageProps: { session, ...pageProps } }) {
    return (
        <div>
            <Component {...pageProps} />
        </div>
        
    );
}

export default MyApp;
