import Str from './Str';
import Arr from './Arr';

/**
 * Kiểm tra 1 giá trị nào đó có tồn tại hay không.
 * @param {mixed} value Giá trị cần kiểm tra
 * @returns bool
 */
export const isset = value => {
    return value !== undefined && value !== null;
};

/**
 * Kiểm tra 1 giá trị nào đó có rỗng hay không.
 * @param {mixed} value Giá trị cần kiểm tra
 * @returns bool
 */
export const empty = value => {
    if (Array.isArray(value)) {
        return value.length === 0;
    }
    return value === undefined || value === null || value === false || value === '';
};

export const _obj = {
    combine: (keys, values) => {
        if (keys.length < values.length) {
            for (let i = 0; i < values.length - keys.length; i++) {
                keys.push(`key_${i}`);
            }
        }
        return keys.reduce(
            (pre, cur, curIndex) => ({
                ...pre,
                [cur]: values?.[curIndex] ? values[curIndex] : null,
            }),
            {}
        );
    },
    get: (obj, keys, defaultValue = null) => {
        let result = obj;
        keys.split('.').forEach(key => {
            result = result?.[key];
        });
        if (!isset(result)) {
            if (defaultValue instanceof Function) return defaultValue();
            return defaultValue !== undefined ? defaultValue : null;
        }
        return result;
    },
};

/**
 * Chuyển object thành query string hoặc query string sang object.
 * @param {string|object} value Object|string cần chuyển đổi
 * @param {string} to Giá trị muốn chuyển sang (string|object)
 * @returns string|object
 */
export const serialize = (value, to = 'string') => {
    const urlParam = new URLSearchParams(value);
    if (to === 'string') {
        return isset(value) ? '?' + urlParam.toString() : '';
    } else if (to.toLowerCase() === 'obj' || to.toLowerCase() === 'object') {
        const entries = urlParam.entries();
        const result = {};
        for (const [key, value] of entries) {
            result[key] = value;
        }
        return result;
    }
};

/**
 * Object hỗ trợ xử lý chuỗi.
 */
export const _str = {
    camel: value => new Str(value).camel().get(),
    kebab: value => new Str(value).kebab().get(),
    lower: value => new Str(value).lower().get(),
    random: (length = 16, options = {}) => new Str().random(length, options),
    snake: value => new Str(value).snake().get(),
    studly: value => new Str(value).studly().get(),
    title: value => new Str(value).title().get(),
    upper: value => new Str(value).title().get(),
    of: value => new Str(value),
};

/**
 * Khởi tạo đối tượng Arr.
 * @param {string} value Giá trị đối tượng cần hỗ trợ
 * @return Arr
 */
export const _arr = value => new Arr(value);

/**
 * Tạo ra 1 chuỗi ngẫu nhiên.
 * @param {int} length Độ dài chuỗi
 * @param {object} options Cấu hình điều kiện để tạo chuỗi
 * @returns string
 */
export const strRandom = (length = 16, options = {}) => new Str().random(length, options);
