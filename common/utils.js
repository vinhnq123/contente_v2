import moment from 'moment';
import localization from 'moment/locale/vi';

export const REGEX_EMAIL = /^[a-zA-Z0-9]+[a-zA-Z0-9_\-\.]+@[a-zA-Z0-9\-_]+\.[a-zA-Z0-9]{2,}$/i;
export const REGEX_BLOCK_QUOTE = /\<blockquote .*?\>(.*?)<\/blockquote\>/g;
export const THUMBNAIL_DEFAULT = '/static/images/thumbnails/default-thumbnail.jpg';

export const emailValid = email => REGEX_EMAIL.test(email);

export const makeSummaryFromContent = (content, length = 100) => {
    const stripedHtml = content.replace(/<[^>]+>/g, '').replace('&#8203;', '');
    if (content.length > length) {
        let result = '';
        const words = stripedHtml.split(' ');
        words.forEach(word => {
            const tmp =
                result +
                word
                    .trim()
                    .replace(/\r?\n|\r| /g, ' ')
                    .replace(/\s\s+/g, ' ') +
                ' ';
            if (tmp.length <= length - 4) {
                result = tmp;
            }
        });
        return result + '...';
    }
    return stripedHtml;
};

export const timeAgo = seconds => {
    const ymd = moment(seconds * 1000).format('YYYYMMDDHHmmss');
    return moment(ymd, 'YYYYMMDDHHmmss').locale('vi', localization).fromNow();
};
