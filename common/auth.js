import cookie from 'js-cookie';

export const accessToken = req => {
    if (!req) {
        return cookie.get('acc_tkn');
    }
    return req?.cookies?.acc_tkn;
};

export const isLoggedIn = req => {
    if (!req) {
        return cookie.get('acc_tkn') && cookie.get('usr_inf');
    }
    const { acc_tkn, usr_inf } = req?.cookies;
    return acc_tkn && usr_inf;
};

export const notLoggedIn = req => !isLoggedIn(req);

export const destroyAuth = () => {
    cookie.remove('acc_tkn');
    cookie.remove('usr_inf');
    cookie.remove('frm_wth');
    cookie.remove('user_id');
};
export const cleanUpCookies = () => {
    if (notLoggedIn()) {
        if (cookie.get('usr_inf') || cookie.get('frm_wth') || cookie.get('user_id')) {
            destroyAuth();
        }
    }
};
